#include "view.h"
#include <QApplication>
#include <QKeyEvent>
#include <iostream>
#include "gameworld/menuscreen.h"
#include "gameworld/gameapplication.h"
#include "engine/core/graphics.h"

View::View(QWidget *parent) : QGLWidget(parent)
{
    Vector2 SIZE = Vector2(16, 9);

    //Initializes Graphics Object
    m_g = new Graphics(this);

    //Starts Camera
    m_camera = new Camera(SIZE, m_g);

    //Initializes Application
    m_app = new GameApplication(this, SIZE, m_camera);

    //Initializes Start Screen
    MenuScreen *gs = new MenuScreen(m_app, SIZE, m_camera, QString("Platformer4! Press Spacebar."), m_g);

    m_app->addScreen(gs);

    // View needs all mouse move events, not just mouse drag events
    setMouseTracking(true);

    // Hide the cursor since this is a fullscreen app
    setCursor(Qt::BlankCursor);

    // View needs keyboard focus
    setFocusPolicy(Qt::StrongFocus);

    // The game loop is implemented using a timer
    connect(&timer, SIGNAL(timeout()), this, SLOT(tick()));
}

View::~View()
{
    delete m_g;
    delete m_app;
    delete m_camera;

}

void View::initializeGL()
{

    // All OpenGL initialization *MUST* be done during or after this
    // method. Before this method is called, there is no active OpenGL
    // context and all OpenGL calls have no effect.

    // Start a timer that will try to get 60 frames per second (the actual
    // frame rate depends on the operating system and other running programs)
    time.start();
    timer.start(1000 / 60);


    // Center the mouse, which is explained more in mouseMoveEvent() below.
    // This needs to be done here because the mouse may be initially outside
    // the fullscreen window and will not automatically receive mouse move
    // events. This occurs if there are two monitors and the mouse is on the
    // secondary monitor.
    QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));

    m_g->initializeGLFunctions();
    m_app->initializeGL(m_g);
}

void View::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // TODO: call your game rendering code here
    m_camera->transform();
    m_app->onDraw(m_g);
    m_app->onUI(m_g);
    glColor3f(1,1,1);
    renderText(10, 20, "FPS: " + QString::number((int) (fps)), this->font());


}

void View::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    m_camera->resize(w, h);
    m_app->resize(w, h);
}

void View::mousePressEvent(QMouseEvent *event)
{
    m_app->mousePressEvent(event);
}

void View::mouseMoveEvent(QMouseEvent *event)
{
    m_app->mouseMoveEvent(event);

}

void View::mouseReleaseEvent(QMouseEvent *event)
{
    m_app->mouseReleaseEvent(event);
}

void View::wheelEvent(QWheelEvent *event)
{
    m_app->wheelEvent(event);
}

void View::keyPressEvent(QKeyEvent *event)
{
    m_app->keyPressEvent(event);

}

void View::keyReleaseEvent(QKeyEvent *event)
{
    m_app->keyReleaseEvent(event);
}

void View::tick()
{
    // Get the number of seconds since the last tick (variable update rate)
    float seconds = time.restart() * 0.001f;
    fps = .02f / seconds + .98f * fps;

    // TODO: Implement the game update here
    m_app->onTick(seconds);
    // Flag this view for repainting (Qt will call paintGL() soon after)
    update();
}
