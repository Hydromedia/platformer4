#ifndef GAMEGEOMETRICMANAGER_H
#define GAMEGEOMETRICMANAGER_H
#include "engine/geometric/geometricmanager.h"
#include "src/obj.h"
#include "engine/geometric/navigationtriangle.h"

class GameGeometricManager : public GeometricManager
{
public:

    GameGeometricManager(Graphics *g, World *w, int level);
    ~GameGeometricManager();
    void onTick(long seconds);
    void onDraw(Graphics *g);
    void loadTerrain();
private:
    int m_level = 1;

};

#endif // GAMEGEOMETRICMANAGER_H
