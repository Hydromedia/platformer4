#ifndef GAMESCREEN_H
#define GAMESCREEN_H
#include "engine/core/screen.h"
#include "gameworld.h"

class GameScreen : public Screen
{
public:
    GameScreen(Application *a, Vector2 size, Camera *c, Graphics *g, int level);
    ~GameScreen();
    void onTick(float nanos) override;
    void onDraw(Graphics *g) override;
    void onUI(Graphics *g) override;
    void onKeyPressed(QKeyEvent *event) override;
    void onMouseDragged(QKeyEvent *event) override;
    void initializeGL(Graphics *g) override;
    void resize(int w, int h) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override ;

    void wheelEvent(QWheelEvent *event) override;

    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
private:
    GameWorld *m_world;
};

#endif // GAMESCREEN_H
