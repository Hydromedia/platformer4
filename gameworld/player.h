#ifndef PLAYER_H
#define PLAYER_H
#include "engine/core/entity.h"
#include "engine/collision/cylinder.h"
#include <QMouseEvent>
#include <QApplication>
#include <QKeyEvent>
#include "gameworld/gameworld.h"
#include "engine/core/camera.h"

class GameWorld;
class Player : public Entity
{
public:
    Player(Screen *screen, Camera *camera, GameWorld *w);
    ~Player();
    void onStaticCollide(Vector3 normal);
    void onTick(float nanos) override;
    void onDraw(Graphics *g) override;
    void onUI(Graphics *g) override;
    void setPosition(Vector3 vector) override;
    void onCollide (Entity* e) override;
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void setGoalVelocityX(float X);
    void setGoalVelocityZ(float Z);
    void mousePressEvent(QMouseEvent *event);
    int health = 3;
private:
    float count = 0;
    bool m_canJump = false;
    bool m_left = false;
    bool m_right = false;
    bool m_forward = false;
    bool m_back = false;
    Vector3 m_direction = Vector3(0,0,0);
    Vector3 m_perpendicular = Vector3(0,0,0);
    Vector3 m_goalVelocity = Vector3(0,0,0);
    Camera *m_camera;
    Screen *m_screen;
    GameWorld *world;
};

#endif // PLAYER_H
