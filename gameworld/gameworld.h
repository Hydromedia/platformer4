#ifndef GAMEWORLD_H
#define GAMEWORLD_H
#include "engine/core/world.h"
#include <qgl.h>
#include <QMouseEvent>
#include <QApplication>
#include "player.h"
#include "engine/voxel/voxelmanager.h";
#include "engine/geometric/geometricmanager.h";
#include "gameworld/gametrianglepather.h"
#include "gameworld/gamegeometricmanager.h"
#include "engine/core/continuousphysicsmanager.h"
#include "gameworld/enemy.h"

class Enemy;
class Player;
class GameWorld : public World
{
public:
    GameWorld(Screen *screen, Camera *camera, Graphics *g, int level);
    ~GameWorld();

    Vector3 nextPosition(Entity *e, Vector3 oldPosition, Vector3 potentialPosition);
    void onTick(float nanos) override;
    void onDraw(Graphics *g) override;
    void onUI(Graphics *g) override;
    void onKeyPressed(QKeyEvent *event) override;
    void onMouseDragged(QKeyEvent *event) override;
    void resize(int w, int h) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override ;

    void wheelEvent(QWheelEvent *event) override;
    void lose();
    void win();
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void newPath(Vector3 start, Vector3 goal);
    void newPathStart(Vector3 start);
    Player *m_player;
    GeometricManager *geometricManager;
    GameTrianglePather *trianglePather;

private:
    Enemy *m_enemy;
    bool drawNavMesh = true;
    //ContinuousPhysicsManager *continuousManager;
    float camVeloc = 0;
    float camPos = 0;
    Ellipsoid *e;
    float count = 0;
};

#endif // GAMEWORLD_H
