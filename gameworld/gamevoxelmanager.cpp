#include "gamevoxelmanager.h"


GameVoxelManager::GameVoxelManager(Graphics *g, World *w) : VoxelManager (g, w)
{
    int seed = 1;
    generator = new PerlinNoiseGenerator3D(seed);
    initializeWorld(10, 2, 10, 32);
}

GameVoxelManager::~GameVoxelManager()
{

}

void GameVoxelManager::initializeChunks(){
    for (int i =0; i < worldSizeX; i++) {
        for (int j =0; j < worldSizeY; j++) {
            for (int u =0; u < worldSizeZ; u++) {
                initializeBlocks(getChunk(i, j, u));
            }
        }
    }
}

void GameVoxelManager::initializeBlocks(Chunk *c){
    for (int i = 0; i < chunkSize; i++) {
        for (int j = 0; j < chunkSize; j++) {
            for (int u = 0; u < chunkSize; u++) {
                //up
                if (j + 1 >= chunkSize) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].topShown = true;
                } else if(c->blocks[i+ chunkSize * (j + 1 + chunkSize * u)].type==1) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].topShown = true;
                }
                //down
                if (j - 1 < 0) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].bottomShown = true;
                } else if (c->blocks[i+ chunkSize * (j - 1 + chunkSize * u)].type==1) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].bottomShown = true;
                }
                //left
                if (i - 1 < 0) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].leftShown = true;
                } else if (c->blocks[i - 1 + chunkSize * (j + chunkSize * u)].type==1) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].leftShown = true;
                }
                //right
                if (i + 1 >= chunkSize) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].rightShown = true;
                } else if (c->blocks[i + 1 + chunkSize * (j + chunkSize * u)].type==1) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].rightShown = true;
                }
                //front
                if (u + 1 >= chunkSize){
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].frontShown = true;
                }else if (c->blocks[i+ chunkSize * (j + chunkSize * (u+1))].type==1) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].frontShown = true;
                }
                //back
                if (u - 1 < 0){
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].backShown = true;
                }else if (c->blocks[i+ chunkSize * (j + chunkSize * (u-1))].type==1) {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].backShown = true;
                }
            }
        }
    }
}

void GameVoxelManager::onTick(long seconds)
{

}

bool GameVoxelManager::chunkIsInView(int i, int j, int z, Graphics *g){
    Vector3 chunkCorners[8];
    chunkCorners[0] =  Vector3((i)*chunkSize, (j)*chunkSize, (z)*chunkSize);
    chunkCorners[1] =  Vector3((i+1)*chunkSize, (j)*chunkSize, (z)*chunkSize);
    chunkCorners[2] =  Vector3((i+1)*chunkSize, (j)*chunkSize, (z+1)*chunkSize);
    chunkCorners[3] =  Vector3((i)*chunkSize, (j)*chunkSize, (z+1)*chunkSize);

    chunkCorners[4] =  Vector3((i)*chunkSize, (j+1)*chunkSize, (z)*chunkSize);
    chunkCorners[5] =  Vector3((i+1)*chunkSize, (j+1)*chunkSize, (z)*chunkSize);
    chunkCorners[6] =  Vector3((i+1)*chunkSize, (j+1)*chunkSize, (z+1)*chunkSize);
    chunkCorners[7] =  Vector3((i)*chunkSize, (j+1)*chunkSize, (z+1)*chunkSize);

    //    for (int p = 0; p < 6; p++){
    //        int cornerCount = 0;
    //        for (int corner = 0; corner < 8; corner++){

    //        }

    //    }
    for (int p = 0; p < 6; p++) {
        Vector4 vec = g->planes->at(p);
        int flag = 0;
        for(int c = 0; c < 8; c++){
            Vector3 cor = chunkCorners[c];
            float n = cor.x*vec.x + cor.y*vec.y + cor.z*vec.z + vec.w;
            if (n < 0){
                flag++;
                if (flag == 8){
                    //std::cout << "FALSE\n" << std::flush;
                    return false;
                }
            }
        }
    }
    //std::cout << "TRUE\n" << std::flush;
    return true;
}

void GameVoxelManager::onDraw(Graphics *g)
{
    int numChunks = 0;
    g->bindTexture("terrain");
    for (int i =0; i < worldSizeX; i++) {
        for (int j =0; j < worldSizeY; j++) {
            for (int u =0; u < worldSizeZ; u++) {
                if (chunkIsInView(i, j, u, g)){
                    numChunks++;
                    //drawBlocks(g, getChunk(i, j, u), i*chunkSize, j*chunkSize, u*chunkSize);
                    getChunk(i, j, u)->onDraw(g);//drawBlocks(g, getChunk(i, j, u), i*chunkSize, j*chunkSize, u*chunkSize);
                }
            }
        }
    }
    //std::cout << numChunks << std::endl;
    g->unbindTexture();
}

void GameVoxelManager::drawBlocks(Graphics *g, Chunk *c, int x, int y, int z) {
//BOTTOM, BACK, LEFT is considered the point given for drawing.
    for (int i =0; i < chunkSize; i++) {
        for (int j =0; j < chunkSize; j++) {
            for (int u =0; u < chunkSize; u++) {
                if (c->blocks[i+ chunkSize * (j + chunkSize * u)].type!=1){
                    g->drawRectangularPrismWithBoundAtlasTexture(x + i, c->blocks[i+ chunkSize * (j + chunkSize * u)].leftShown,
                                                                 x + i + 1, c->blocks[i+ chunkSize * (j + chunkSize * u)].rightShown,
                                                                 u+z +1, c->blocks[i+ chunkSize * (j + chunkSize * u)].frontShown,
                                                                 u+z, c->blocks[i+ chunkSize * (j + chunkSize * u)].backShown,
                                                                 y + j+1, c->blocks[i+ chunkSize * (j + chunkSize * u)].topShown,
                                                                 y+j, c->blocks[i+ chunkSize * (j + chunkSize * u)].bottomShown,
                                                                 c->blocks[i+ chunkSize * (j + chunkSize * u)].type);
                }
            }
        }
    }
}

void GameVoxelManager::generateTerrain()
{
    float *startArray;
    startArray = new float[chunkSize*worldSizeX*chunkSize*worldSizeZ];
    for (int i =0; i < chunkSize*worldSizeX; i++) {
        for (int j =0; j < chunkSize*worldSizeZ; j++) {
            startArray[i + j * chunkSize*worldSizeZ] = generator->perlin3D(i, j, 4);
        }
    }
    for (int i =0; i < worldSizeX; i++) {
        for (int j =0; j < worldSizeY; j++) {
            for (int u =0; u < worldSizeZ; u++) {
                generateBlocks(getChunk(i, j, u), i*chunkSize, j*chunkSize, u*chunkSize, startArray);
            }
        }
    }
    //chunks[5]->blocks[5].type = 15;
}

void GameVoxelManager::generateBlocks(Chunk *c, int x, int y, int z, float *startArray){
    for (int i =0; i < chunkSize; i++) {
        for (int j =0; j < chunkSize; j++) {
            for (int u =0; u < chunkSize; u++) {
                float noiseVal = startArray[(int)(i+x+chunkSize*worldSizeX*(u+z))];
                int type = 1;
                if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > .8){
                    type = 1;
                } else if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > .6) {
                    type = 15;
                } else if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > .4) {
                    type = 14;
                } else if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > .2) {
                    type = 13;
                } else if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > 0) {
                    type = 12;
                } else if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > -.2) {
                    type = 11;
                } else if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > -.4) {
                    type = 10;
                } else if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > -.6) {
                    type = 9;
                } else if (((float)(j + y))/((float)(chunkSize*worldSizeY)) > -.8) {
                    type = 8;
                } else {
                    type = 15;
                }

                float i234ij = ((float)(j + y))/((float)(chunkSize*worldSizeY));
                //std::cout << i234ij << std::flush;
                //c->blocks[i+ chunkSize * (j + chunkSize * u)].type = type;
                if (((float)(j + y))/((float)(chunkSize*worldSizeY)) <= ((float)(noiseVal + 1))/((float)2)){
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].type = type;
                } else {
                    c->blocks[i+ chunkSize * (j + chunkSize * u)].type = 1;
                }
                //std::cout<< noiseVal << std::flush;
            }
        }
    }
}

