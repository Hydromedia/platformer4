#ifndef MENUSCREEN_H
#define MENUSCREEN_H
#include "engine/core/application.h"


class MenuScreen : public Screen
{
public:
    MenuScreen(Application *a, Vector2 size, Camera *c, QString s, Graphics *g);
    ~MenuScreen();
    void onTick(float nanos) override;
    void onDraw(Graphics *g) override;
    void onKeyPressed(QKeyEvent *event) override;
    void onMouseDragged(QKeyEvent *event) override;
    void initializeGL(Graphics *g) override;
    void resize(int w, int h) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override ;

    void wheelEvent(QWheelEvent *event) override;

    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
private:
    QString m_string;
};

#endif // MENUSCREEN_H
