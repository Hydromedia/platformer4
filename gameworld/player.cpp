#include "player.h"

Player::Player(Screen *screen, Camera *camera, GameWorld *w) : Entity(w)
{
    m_camera = camera;
    this->m_collisionShape = new Cylinder(this, 1, 2.5);
    this->m_position = Vector3(1,2,0);
    this->boundingAABBSize = Vector3(.5, 1, .5);
    this->m_geometricEllipsoid = Ellipsoid(Vector3(0,0,0), Vector3(0,0,0), Vector3(.5,1,.5));
    m_screen = screen;
    world = w;
    //world->newPath(this->getPosition(), Vector3(0,1,0));
    //world->newPathStart(this->getPosition());

}

Player::~Player()
{
    delete m_collisionShape;
}

void Player::onStaticCollide(Vector3 normal)
{
    if (normal.y > 0) {
        m_canJump = true;
    } else {
        //m_canJump = false;
    }
}


void Player::onTick(float nanos){
    count+=nanos;
    if (count >= 15){
        world->win();
        return;
    } else if (health <= 0) {
        world->lose();
    }
    m_canJump = false;
    Entity::onTick(nanos);
    float yaw = m_camera->getYaw();
    Vector3 dir = Vector3(cos(yaw), 0, sin(yaw)); // forward-backward movement
    Vector3 perp = Vector3(dir.z, 0, -dir.x); // strafe movement
    m_direction = dir;
    m_perpendicular = perp;
    if (m_forward) {
        m_goalVelocity += 10*m_direction;
    } if (m_back) {
        m_goalVelocity += 10*(-m_direction);
    } if (m_left){
        m_goalVelocity += 10*m_perpendicular;
    } if (m_right){
        m_goalVelocity += 10*(-m_perpendicular);
    }
    //m_goalVelocity = m_direction;
    m_force += .02f* Vector3((m_goalVelocity.x - (this->m_velocity.x)), 0, (m_goalVelocity.z - (this->m_velocity.z)));
    m_goalVelocity = Vector3(0,0,0);
}

void Player::onDraw(Graphics *g){
    if (m_camera->getMode()){
        //m_collisionShape->onDraw(g);
//        float width = boundingAABBSize.x;
//        float height = boundingAABBSize.y;
//        float depth = boundingAABBSize.z;
//        g->drawRectangularPrismWithBoundAtlasTexture(m_position.x-width, true, m_position.x+width, true,
//                                                     m_position.z + depth, true, m_position.z - depth, true,
//                                                     m_position.y+height, true, m_position.y - height, true, 14);
        glColor3f(1, .5, .5);
        GLUquadric *quad;
        quad = gluNewQuadric();
        glPushMatrix();
        glTranslatef(this->m_position.x, this->m_position.y, this->m_position.z);
        glScalef(this->m_geometricEllipsoid.radius.x, this->m_geometricEllipsoid.radius.y, this->m_geometricEllipsoid.radius.z);
        gluSphere(quad, 1, 32, 16);
        glPopMatrix();
    }
    m_camera->setPosition(Vector3(this->getPosition().x, this->getPosition().y + 2, this->getPosition().z));

}

void Player::onUI(Graphics *g){

}

void Player::setPosition(Vector3 vector){
    this->m_position = vector;
    m_camera->setPosition(Vector3(this->getPosition().x, this->getPosition().y + 2, this->getPosition().z));
}

void Player::onCollide (Entity* e) {

}

void Player::keyPressEvent(QKeyEvent *event)
{
    // TODO: Handle keyboard presses here

    if(event->key() == Qt::Key_W){
        m_forward = true;
        //std::cout << "W" <<std::endl;
    }

    if(event->key() == Qt::Key_S){
        m_back = true;
        //std::cout << "S" <<std::endl;
    }

    if(event->key() == Qt::Key_A){
        m_left = true;
        //std::cout << "A" <<std::endl;
    }

    if(event->key() == Qt::Key_D){
        m_right = true;
        //std::cout << "D" <<std::endl;
    }

    if(event->key() == Qt::Key_Space) {
        if (m_canJump){
            m_position.y += .01;
            m_force.y = 1;
            m_velocity.y = 10;
        }
     }

    if(event->key() == Qt::Key_G) {
        world->newPathStart(this->getPosition());
     }
}

void Player::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_W){
        m_forward = false;
        //std::cout << "W1" <<std::endl;
        //m_goalVelocity = 100*dir;// * Vector3(m_goalVelocity.x, m_goalVelocity.y, 5);
    }

    if(event->key() == Qt::Key_S){
        m_back = false;
        //std::cout << "S1" <<std::endl;
         //m_goalVelocity = 100*(-dir);// * Vector3(m_goalVelocity.x, m_goalVelocity.y, 5);
    }

    if(event->key() == Qt::Key_A){
        m_left = false;
        //std::cout << "A1" <<std::endl;
        //m_goalVelocity = 100*perp;// * Vector3(5, m_goalVelocity.y, m_goalVelocity.z);
    }

    if(event->key() == Qt::Key_D){
        m_right = false;
        //std::cout << "D1" <<std::endl;
        //m_goalVelocity =  100*(-perp);// * Vector3(5, m_goalVelocity.y, m_goalVelocity.z);
    }
}

void Player::mouseMoveEvent(QMouseEvent *event)
{
    int deltaX = event->x() - m_screen->app->_view()->width() / 2;
    int deltaY = event->y() - m_screen->app->_view()->height() / 2;
    if (!deltaX && !deltaY) return;
    QCursor::setPos(m_screen->app->_view()->mapToGlobal(QPoint(m_screen->app->_view()->width() / 2, m_screen->app->_view()->height() / 2)));

    // TODO: Handle mouse movements here
    m_camera->rotate(deltaX / 100.f, deltaY / 100.f);
}

void Player::setGoalVelocityX(float X)
{

}

void Player::setGoalVelocityZ(float Z)
{

}

void Player::mousePressEvent(QMouseEvent *event)
{

}
