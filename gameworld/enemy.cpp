#include "enemy.h"

Enemy::Enemy(Screen *screen, GameWorld *w) : Entity(w)
{
    this->m_collisionShape = new Cylinder(this, 1, 2.5);
    this->m_position = Vector3(5,5,5);
    this->boundingAABBSize = Vector3(.5, 1, .5);
    this->m_geometricEllipsoid = Ellipsoid(Vector3(0,0,0), Vector3(0,0,0), Vector3(.5,1,.5));
    m_screen = screen;
    world = w;
    //world->newPath(this->getPosition(), Vector3(0,1,0));
    //world->newPathStart(this->getPosition());

}

Enemy::~Enemy()
{
    delete m_collisionShape;
}

void Enemy::onStaticCollide(Vector3 normal)
{

}

void Enemy::onTick(float nanos){
    count +=nanos;
    if (count >= 1) {
        count = 0;
        world->newPath(this->getPosition(), world->m_player->getPosition());
        QList<Vector3> path = world->trianglePather->getPath();
        if (path.length() > 1){
            Vector3 thing = path[1]-this->m_position;
            thing.normalize();
            this->m_velocity = thing*4;
        }
    }
    float vec = ((world->m_player->getPosition()) - (this->m_position)).length();
    //std::cout << vec << std::endl;
    count2 += nanos;

    if (count2 >=1) {
        drawLaser = false;
    }
    if (vec < 5 && count2 >= 3) {
        drawLaser = true;
        laserStart = this->getPosition();
        laserEnd = world->m_player->getPosition();
        count2 = 0;
        Vector3 thing = ((world->m_player->getPosition()) - (this->m_position));
        thing.normalize();
        world->m_player->m_impulse += thing*10;
        world->m_player->health--;
    }
    Entity::onTick(nanos);
}

void Enemy::onDraw(Graphics *g){
    glColor3f(.5, 1, 1);
    GLUquadric *quad;
    quad = gluNewQuadric();
    glPushMatrix();
    glTranslatef(this->m_position.x, this->m_position.y, this->m_position.z);
    glScalef(this->m_geometricEllipsoid.radius.x, this->m_geometricEllipsoid.radius.y, this->m_geometricEllipsoid.radius.z);
    gluSphere(quad, 1, 32, 16);
    glPopMatrix();

    //draw laser
    if (drawLaser) {
        glColor3f(0, .765, .45);
        glLineWidth(4);
        glBegin(GL_LINES);
          glVertex3f(laserStart.x, laserStart.y, laserStart.z);
          glVertex3f(laserEnd.x, laserEnd.y, laserEnd.z);
        glEnd();
        glLineWidth(1);
    }
}

void Enemy::onUI(Graphics *g){
    std::cout << "Getting Called" << std::endl;

    int view[4]; double model[16], proj[16];
    glGetDoublev(GL_MODELVIEW_MATRIX, model);
    glGetDoublev(GL_PROJECTION_MATRIX, proj);
    glGetIntegerv(GL_VIEWPORT, view);
    double sx, sy, sz; // to-be screen-space coordinates
    gluProject(this->getPosition().x, this->getPosition().y, this->getPosition().z, model, proj, view, &sx, &sy, &sz);

    Vector3 test = (Vector3(sx, sy, sz));
    std::cout << test << std::endl;


    g->setUpScreenProjection();


    //... drawing code
    int iter = 3 - world->m_player->health;
    while(iter > 0){
        glColor3f(1, 0, 0);
        glLineWidth(4);
        glBegin(GL_LINES);
          glVertex2f(sx+iter*10, sy);
          glVertex2f(sx+iter*10, sy+15);
        glEnd();
        glLineWidth(1);

        iter--;
    }
    g->undoScreenProjection();
}

void Enemy::setPosition(Vector3 vector){
    this->m_position = vector;
}

void Enemy::onCollide (Entity* e) {

}
