#include "gametrianglepather.h"

GameTrianglePather::GameTrianglePather(Graphics *g, World *w, Vector3 start, Vector3 goal, Ellipsoid *ellipse, int level, QList<Triangle*> *tris): TrianglePather (g, w, start, goal, ellipse, tris, level)
{

}

GameTrianglePather::~GameTrianglePather()
{

}

void GameTrianglePather::onDraw(Graphics *g)
{

//    glColor3f(1, .5, .2);
//    GLUquadric *quad;
//    quad = gluNewQuadric();
//    glPushMatrix();
//    glTranslatef(m_drawEnd.x, m_drawEnd.y, m_drawEnd.z);
//    glScalef(.5, 1, .5);
//    gluSphere(quad, 1, 32, 16);
//    glPopMatrix();

//    //draw mesh triangles
//    glColor4f(0, 1, 0, .5);
//    g->drawObj(0 - m_level, .5f);

//    //draw path triangles
//    for (int i = 0; i < pathTriangles.size(); i++) {
//        glBegin(GL_TRIANGLES);
//        glColor3f(0.1, 0.5, 0.8);
//            glVertex3f(pathTriangles.at(i)->vertices[0].x, pathTriangles.at(i)->vertices[0].y + .75f, pathTriangles.at(i)->vertices[0].z);
//            glVertex3f(pathTriangles.at(i)->vertices[1].x, pathTriangles.at(i)->vertices[1].y + .75f, pathTriangles.at(i)->vertices[1].z);
//            glVertex3f(pathTriangles.at(i)->vertices[2].x, pathTriangles.at(i)->vertices[2].y + .75f, pathTriangles.at(i)->vertices[2].z);
//        glEnd();

//        glColor3f(0, .5, .5);
//        glLineWidth(3);
//        glBegin(GL_LINES);
//            glVertex3f(pathTriangles.at(i)->vertices[0].x, pathTriangles.at(i)->vertices[0].y + .75f, pathTriangles.at(i)->vertices[0].z);
//            glVertex3f(pathTriangles.at(i)->vertices[2].x, pathTriangles.at(i)->vertices[2].y + .75f, pathTriangles.at(i)->vertices[2].z);
//        glEnd();
//        glLineWidth(1);

//        glColor3f(0, .5, .5);
//        glLineWidth(3);
//        glBegin(GL_LINES);
//            glVertex3f(pathTriangles.at(i)->vertices[1].x, pathTriangles.at(i)->vertices[1].y + .75f, pathTriangles.at(i)->vertices[1].z);
//            glVertex3f(pathTriangles.at(i)->vertices[2].x, pathTriangles.at(i)->vertices[2].y + .75f, pathTriangles.at(i)->vertices[2].z);
//        glEnd();
//        glLineWidth(1);

//        glColor3f(0, .5, .5);
//        glLineWidth(3);
//        glBegin(GL_LINES);
//            glVertex3f(pathTriangles.at(i)->vertices[0].x, pathTriangles.at(i)->vertices[0].y + .75f, pathTriangles.at(i)->vertices[0].z);
//            glVertex3f(pathTriangles.at(i)->vertices[1].x, pathTriangles.at(i)->vertices[1].y + .75f, pathTriangles.at(i)->vertices[1].z);
//        glEnd();
//        glLineWidth(1);
//        //draw path
//        for (int i = 0; i < currentPath.length(); i++){
//            if (i < currentPath.length() - 1) {
//                glColor3f(1, 0, 0);
//                glLineWidth(5);
//                glBegin(GL_LINES);
//                  glVertex3f(currentPath.at(i).x, currentPath.at(i).y+1, currentPath.at(i).z);
//                  glVertex3f(currentPath.at(i+1).x, currentPath.at(i+1).y+1, currentPath.at(i+1).z);
//                glEnd();
//                glLineWidth(1);
//            }
//        }

        //draw portals
//        NavigationTriangle *curr = debugEnd;
//        while(curr != 0){
//            glColor3f(0, .765, .45);
//            glLineWidth(2);
//            glBegin(GL_LINES);
//              glVertex3f(curr->edgeToPrev.a.x, curr->edgeToPrev.a.y+.8, curr->edgeToPrev.a.z);
//              glVertex3f(curr->edgeToPrev.b.x, curr->edgeToPrev.b.y+.8, curr->edgeToPrev.b.z);
//            glEnd();
//            glLineWidth(1);
//            curr = curr->prev;
//        }

//    }
}


