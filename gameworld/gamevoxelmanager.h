#ifndef GAMEVOXELMANAGER_H
#define GAMEVOXELMANAGER_H
#include "engine/voxel/voxelmanager.h";
#include "engine/generation/perlinnoisegenerator3d.h"


class GameVoxelManager : public VoxelManager
{
public:
    GameVoxelManager(Graphics *g, World *w);
    ~GameVoxelManager();
    void onTick(long seconds);
    void onDraw(Graphics *g);
    void generateTerrain();
    void generateBlocks(Chunk *c, int x, int y, int z, float *startArray);
    void drawBlocks(Graphics *g, Chunk *c, int x, int y, int z);
    void initializeChunks();
    void initializeBlocks(Chunk *c);
    bool chunkIsInView(int i, int j, int z, Graphics *g);
private:
    PerlinNoiseGenerator3D * generator;
};

#endif // GAMEVOXELMANAGER_H
