#include "gamescreen.h"
#include <qgl.h>
#include <QTime>
#include <QTimer>
#include <QMouseEvent>
#include <QApplication>
#include <iostream>
#include <math.h>


GameScreen::GameScreen(Application *a, Vector2 size, Camera *c, Graphics *g, int level) : Screen (a, size, c, g)
{
    m_world = new GameWorld(this, m_camera, m_graphics, level);
}

GameScreen::~GameScreen()
{
    delete m_world;
}

void GameScreen::onTick(float nanos)
{
    m_world->onTick(nanos);

}

void GameScreen::onDraw(Graphics *g)
{
    m_world->onDraw(g);
}

void GameScreen::onUI(Graphics *g){
    std::cout << "Game Screen onUI" << std::endl;
    m_world->onUI(g);
}

void GameScreen::onKeyPressed(QKeyEvent *event)
{
    m_world->onKeyPressed(event);
}

void GameScreen::onMouseDragged(QKeyEvent *event)
{
    m_world->onMouseDragged(event);
}

void GameScreen::initializeGL(Graphics *g)
{

}

void GameScreen::resize(int w, int h)
{
    m_world->resize(w,h);
}

void GameScreen::mousePressEvent(QMouseEvent *event)
{
    m_world->mousePressEvent(event);
}

void GameScreen::mouseMoveEvent(QMouseEvent *event)
{
    m_world->mouseMoveEvent(event);
}

void GameScreen::mouseReleaseEvent(QMouseEvent *event)
{
    m_world->mouseReleaseEvent(event);
}

void GameScreen::wheelEvent(QWheelEvent *event)
{
    m_world->wheelEvent(event);
}

void GameScreen::keyPressEvent(QKeyEvent *event)
{
    m_world->keyPressEvent(event);
}

void GameScreen::keyReleaseEvent(QKeyEvent *event)
{
    m_world->keyReleaseEvent(event);
}

