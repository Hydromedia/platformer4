#ifndef GAMETRIANGLEPATHER_H
#define GAMETRIANGLEPATHER_H
#include "engine/geometric/trianglepather.h"
#include "src/obj.h"


class GameTrianglePather : public TrianglePather
{
public:
    GameTrianglePather(Graphics *g, World *w, Vector3 start, Vector3 goal, Ellipsoid *ellipse, int level, QList<Triangle*> *tris);
    ~GameTrianglePather();
    void onDraw(Graphics *g);
private:

};

#endif // GAMETRIANGLEPATHER_H
