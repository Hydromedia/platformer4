#include "gameworld.h"

#include "menuscreen.h"

GameWorld::GameWorld(Screen *screen, Camera *camera, Graphics *g, int level): World (screen, camera, g)
{
    m_screen = screen;
    //continuousManager = new ContinuousPhysicsManager(g, this);
    geometricManager = new GameGeometricManager (g, this, level);
    e = new Ellipsoid(Vector3(0,0,0), Vector3(0,0,0), Vector3(.5,1,.5));
    std::cout << "Level: " << level << std::endl;
    trianglePather = new GameTrianglePather(g, this, Vector3(0,0,0), Vector3(0,0,0), e, level, &geometricManager->triangles);
    m_managers->append(geometricManager);
    m_player = new Player(screen, camera, this);
    this->addEntity(m_player);

    m_enemy = new Enemy(screen, this);
    this->addEntity(m_enemy);
}

GameWorld::~GameWorld()
{
    delete geometricManager;
    delete m_player;
    delete e;
    delete trianglePather;
}

Vector3 GameWorld::nextPosition(Entity *e, Vector3 oldPosition, Vector3 potentialPosition)
{
    //std::cout << "Got to gameworld next pos" << std::endl;
    return geometricManager->collide(e, oldPosition, potentialPosition);

}


void GameWorld::onTick(float nanos){
    //voxelManager->onTick(nanos);

    World::onTick(nanos);
    foreach(Entity *e, *m_entities)
    {
        e->m_force += Vector3(0, (-.3 * e->m_mass), 0);
    }

}

void GameWorld::lose() {
    if (m_screen){
        MenuScreen *gs = new MenuScreen(m_screen->app, m_screen->m_size, m_camera, QString("You Lose! Press Spacebar."), m_graphics);
        m_screen->app->removeTopScreen();
        m_screen->app->addScreen(gs);
    }
}

void GameWorld::win() {
    MenuScreen *gs = new MenuScreen(m_screen->app, m_screen->m_size, m_camera, QString("You Win! Press Spacebar."), m_graphics);
    m_screen->app->removeTopScreen();
    m_screen->app->addScreen(gs);
}

void GameWorld::onDraw(Graphics *g){
    g->updateView();
    geometricManager->onDraw(g);
    World::onDraw(g);

    if (drawNavMesh){
        trianglePather->onDraw(g);
    }
}

void GameWorld::onUI(Graphics *g){
    std::cout << "Game World onUI" << std::endl;
    World::onUI(g);
}

void GameWorld::onKeyPressed(QKeyEvent *event) {

}

void GameWorld::onMouseDragged(QKeyEvent *event){

}

void GameWorld::resize(int w, int h){

}

void GameWorld::mousePressEvent(QMouseEvent *event){
    m_player->mousePressEvent(event);
}

void GameWorld::mouseMoveEvent(QMouseEvent *event){
    m_player->mouseMoveEvent(event);
    // This starter code implements mouse capture, which gives the change in
    // mouse position since the last mouse movement. The mouse needs to be
    // recentered after every movement because it might otherwise run into
    // the edge of the screen, which would stop the user from moving further
    // in that direction. Note that it is important to check that deltaX and
    // deltaY are not zero before recentering the mouse, otherwise there will
    // be an infinite loop of mouse move events.
}

void GameWorld::mouseReleaseEvent(QMouseEvent *event){

}

void GameWorld::wheelEvent(QWheelEvent *event){

}
void GameWorld::keyPressEvent(QKeyEvent *event){
    World::keyPressEvent(event);
    if (event->key() == Qt::Key_Escape) QApplication::quit();
    if (event->key() == Qt::Key_Backspace){
        MenuScreen *gs = new MenuScreen(m_screen->app, m_screen->m_size, m_camera, QString("Platformer4! Press Spacebar."), m_graphics);
        m_screen->app->removeTopScreen();
        m_screen->app->addScreen(gs);
    }
    if (event->key() == Qt::Key_M){
        drawNavMesh = !drawNavMesh;
    }

    m_player->keyPressEvent(event);
}

void GameWorld::keyReleaseEvent(QKeyEvent *event){
    m_player->keyReleaseEvent(event);
}

void GameWorld::newPath(Vector3 start, Vector3 goal)
{
    this->trianglePather->setStartAndGoal(start, goal);
    this->trianglePather->generatePath();
}

void GameWorld::newPathStart(Vector3 start)
{
    this->trianglePather->setStart(start);
    this->trianglePather->generatePath();
}

