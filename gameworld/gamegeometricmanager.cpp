#include "gamegeometricmanager.h"


GameGeometricManager::GameGeometricManager(Graphics *g, World *w, int level) : GeometricManager (g, w)
{
    m_level = level;
    m_graphics = g;
    loadTerrain();
}

GameGeometricManager::~GameGeometricManager()
{

}

void GameGeometricManager::onTick(long seconds){

}

void GameGeometricManager::onDraw(Graphics *g)
{
    if (m_level == 1) {
        g->bindTexture("easy");
    } else if (m_level == 2) {
        g->bindTexture("hard");
    } else if (m_level == 3) {
       g->bindTexture("island");
    }
    m_graphics->drawObj(m_level, 0);
    g->unbindTexture();

//    glPushMatrix();
//    Vector3 translateVec = entityPos;//info.normal*50+e->getPosition()-(e->getPosition()/2);
//    glTranslatef(translateVec.x, translateVec.y, translateVec.z);
//    GLUquadric *quadObj = gluNewQuadric();
//    Vector3 vec = Vector3(-1,1,1);
//    vec.normalize();
//    glRotatef(120, vec.x, vec.y, vec.z);
//    gluCylinder(quadObj, 6, 6, 50, 36, 36);
//    glPopMatrix();

    //std::cout << "Actuall being called" << std::endl;
}

void GameGeometricManager::loadTerrain(){

    for (int i = 0; i<m_graphics->m_levelNumberToOBJ->value(m_level)->triangles.size(); i++){
        Triangle *tri = new Triangle(m_graphics->m_levelNumberToOBJ->value(m_level)->vertices.at(m_graphics->m_levelNumberToOBJ->value(m_level)->triangles.at(i).a.vertex),
                                m_graphics->m_levelNumberToOBJ->value(m_level)->vertices.at(m_graphics->m_levelNumberToOBJ->value(m_level)->triangles.at(i).b.vertex),
                                m_graphics->m_levelNumberToOBJ->value(m_level)->vertices.at(m_graphics->m_levelNumberToOBJ->value(m_level)->triangles.at(i).c.vertex));
        triangles.push_front(tri);
    }
    //std::cout << "Actuall READING in called" << std::endl;
}
