#include "gameapplication.h"

GameApplication::GameApplication(View *v, Vector2 size, Camera *c) : Application (v, size, c)
{
}

void GameApplication::initializeGL(Graphics *g)
{
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glEnable(GL_CULL_FACE);
//    glCullFace(GL_FRONT);
    //loading textures

    g->loadTexture(":/images/res/level_easy.png", "easy");

    g->loadTexture(":/images/res/level_hard.png", "hard");

    g->loadTexture(":/images/res/level_island.png", "island");

//    g->loadTexture(":/images/res/grass.png", "grass");
//    g->loadTexture(":/images/res/terrain.png", "terrain");
//    g->m_atlasIndexAmount = ((float)16)/((float)256);
//    for (int i = 0; i < 16; i++) {
//        for (int j = 0; j < 16; j++) {
//            g->addToTileCoordinateMap(i * 16 + j, Vector2(((float) i)*(g->m_atlasIndexAmount), ((float) j)*(g->m_atlasIndexAmount)));
//        }
//    }

    //level meshes
    OBJ *o1 = new OBJ();
    g->addToLevelObj(1, o1);

    OBJ *o2 = new OBJ();
    g->addToLevelObj(2, o2);

    OBJ *o3 = new OBJ();
    g->addToLevelObj(3, o3);

    g->readObj(":/images/res/level_easy.obj", 1);
    g->readObj(":/images/res/level_hard.obj", 2);
    g->readObj(":/images/res/level_island.obj", 3);

    //nav meshes
    OBJ *o4 = new OBJ();
    g->addToLevelObj(-1, o4);

    OBJ *o5 = new OBJ();
    g->addToLevelObj(-2, o5);

    OBJ *o6 = new OBJ();
    g->addToLevelObj(-3, o6);

    g->readObj(":/images/res/level_easy_navmesh.obj", -1);
    g->readObj(":/images/res/level_hard_navmesh.obj", -2);
    g->readObj(":/images/res/level_island_navmesh.obj", -3);

}
