#ifndef PERLINNOISEGENERATOR3D_H
#define PERLINNOISEGENERATOR3D_H


class PerlinNoiseGenerator3D
{
public:
    PerlinNoiseGenerator3D(int s);
    ~PerlinNoiseGenerator3D();
    float noise(float x, float z);
    float smoothNoise(float x, float z);
    float interpolatedNoise(float x, float z);
    float perlin3D(float x, float z, float wavelength);
    float cosineInterpolate(float a, float b, float x);
private:
    int seed;


};

#endif // PERLINNOISEGENERATOR3D_H
