#ifndef MANAGER_H
#define MANAGER_H
#include "engine/core/graphics.h"
#include "engine/core/entity.h"
#include "engine/core/world.h"

class World;
class Entity;
class Manager
{
public:
    Manager(Graphics *g, World *w);
    ~Manager();
    virtual void onTick(float nanos);
    virtual Vector3 collide(Entity *e, Vector3 oldPosition, Vector3 potentialPosition) = 0;
    //virtual void addEntity(Entity *e);
    //virtual void removeEntity(Entity *e);
protected:
    World *m_world;
    Graphics *m_graphics;
};

#endif // MANAGER_H
