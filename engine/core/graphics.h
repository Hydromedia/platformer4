#ifndef GRAPHICS_H
#define GRAPHICS_H
#include "vector.h"
#include "view.h"
#include "engine/core/application.h"
#include <QHash>
#include <QGLFunctions>
#include "src/obj.h"


class Graphics : public QGLFunctions
{
public:
    Graphics(QGLWidget *v);
    ~Graphics();

    void setUpScreenProjection();
    void undoScreenProjection();
    void drawQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4);
    //QGLFunctions qGLFuncs = QGLFunctions();
    void drawTexturedQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, QString name);
    void loadTexture(QString path, QString name);
    void drawCylinder(Vector3 translationVector, float topRadius, float botRadius, float height, int slices, int stacks);
    void bindTexture(QString name);
    void unbindTexture();
    void drawQuadWithBoundAtlasTexture(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, char c);
    void addToTileCoordinateMap(char c, Vector2 vec);
    void drawRectangularPrismWithBoundAtlasTexture(float left, bool isLeft, float right, bool isRight,
                                                   float front, bool isFront, float back, bool isBack,
                                                   float top, bool isTop, float bottom, bool isBottom, char c);

    void updateView();
    void updateViewPlanes(Vector4 negativeX, Vector4 positiveX, Vector4 negativeY, Vector4 positiveY, Vector4 negativeZ, Vector4 positiveZ);
    float m_atlasIndexAmount = 0;
    QHash<char, Vector2> *m_atlasTileToCoordinate;
    void initializeGL();
    void readObj(QString s, int level);
    void drawObj(int level, float yOffset);
    QList<Vector4> *planes = new QList<Vector4>();
    Vector4 negX = Vector4(0,0,0,0);
    Vector4 posX= Vector4(0,0,0,0);
    Vector4 negY= Vector4(0,0,0,0);
    Vector4 posY= Vector4(0,0,0,0);
    Vector4 negZ= Vector4(0,0,0,0);
    Vector4 posZ= Vector4(0,0,0,0);

    void addToLevelObj(int level, OBJ *o);
    QHash<int, OBJ*> *m_levelNumberToOBJ;


private:
    QHash<QString, int> *m_textureNameToID;
    QGLWidget *m_view;

};

#endif // GRAPHICS_H
