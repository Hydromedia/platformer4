#include "screen.h"
#include <iostream>

Screen::Screen(Application *a, Vector2 size, Camera *c, Graphics *g)
{
    m_graphics = g;
    std::cout << "CALLING SCREEN CONS" << std::endl;
    app = a;
    m_camera = c;
    m_size = size;
}

Screen::~Screen()
{

}

void Screen::onTick(float nanos)
{

}

void Screen::onDraw(Graphics *g)
{
    std::cout << "Screen Draw";
}

void Screen::onUI(Graphics *g){
    std::cout << "Screen UI" << std::endl;
}

void Screen::onKeyPressed(QKeyEvent *event)
{

}

void Screen::onMouseDragged(QKeyEvent *event)
{

}

void Screen::initializeGL(Graphics *g)
{

}

void Screen::resize(int w, int h)
{

}

void Screen::mousePressEvent(QMouseEvent *event)
{

}

void Screen::mouseMoveEvent(QMouseEvent *event)
{

}

void Screen::mouseReleaseEvent(QMouseEvent *event)
{

}

void Screen::wheelEvent(QWheelEvent *event)
{

}

void Screen::keyPressEvent(QKeyEvent *event)
{

}

void Screen::keyReleaseEvent(QKeyEvent *event)
{

}

