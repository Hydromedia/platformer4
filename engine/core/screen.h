#ifndef SCREEN_H
#define SCREEN_H
#include "graphics.h"
#include "camera.h"
#include "application.h"

class Application;
class Camera;
class Graphics;
class Screen
{
public:
    Screen(Application *a, Vector2 size, Camera *c, Graphics *g);
    ~Screen();

    Application *app;
    Vector2 m_size;
    virtual void onTick(float nanos) = 0;
    virtual void onDraw(Graphics *g) = 0;
    virtual void onUI(Graphics *g);
    virtual void onKeyPressed(QKeyEvent *event) = 0;
    virtual void onMouseDragged(QKeyEvent *event) = 0;
    virtual void initializeGL(Graphics *g) = 0;
    virtual void resize(int w, int h) = 0;

    virtual void mousePressEvent(QMouseEvent *event) = 0;
    virtual void mouseMoveEvent(QMouseEvent *event) = 0;
    virtual void mouseReleaseEvent(QMouseEvent *event) = 0;

    virtual void wheelEvent(QWheelEvent *event) = 0;

    virtual void keyPressEvent(QKeyEvent *event = 0);
    virtual void keyReleaseEvent(QKeyEvent *event) = 0;
protected:
    Graphics *m_graphics;
    Camera *m_camera;
    Screen *m_screen;
};

#endif // SCREEN_H
