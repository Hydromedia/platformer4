#ifndef WORLD_H
#define WORLD_H
#include "graphics.h"
#include "screen.h"
#include "view.h"
#include <qgl.h>
#include "entity.h"
#include "engine/collision/shape.h"
#include "vector.h"
#include <QKeyEvent>
#include "manager.h"

class Manager;
class World
{
public:
    World(Screen *screen, Camera *camera, Graphics *g);
    ~World();

    QList<Entity*> *m_entities;

    virtual Vector3 nextPosition(Entity *e, Vector3 oldPosition,Vector3 potentialPosition) = 0;
    virtual void addEntity(Entity *e);
    virtual void removeEntity(Entity *e);

    virtual void onTick(float nanos);
    virtual void onDraw(Graphics *g);
    virtual void onUI(Graphics *g);
    virtual void resize(int w, int h);

    virtual void onKeyPressed(QKeyEvent *event);
    virtual void onMouseDragged(QKeyEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);

    virtual void wheelEvent(QWheelEvent *event);

    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
protected:
    Graphics *m_graphics;
    Screen *m_screen = NULL;
    Camera *m_camera;
    QList<Manager*> *m_managers;
};

#endif // WORLD_H
