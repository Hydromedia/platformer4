#ifndef ENTITY_H
#define ENTITY_H
#include "graphics.h"
#include "screen.h"
#include "engine/collision/shape.h"
#include "view.h"
#include <qgl.h>
#include "world.h"
#include "engine/geometric/ellipsoid.h"

class Shape;
class World;
class Entity
{
public:
    Entity(World *w);
    ~Entity();

    Vector3 m_velocity = Vector3(0,0,0);
    Vector3 m_acceleration = Vector3(0,0,0);
    Vector3 m_force = Vector3(0,0,0);
    Vector3 m_impulse = Vector3(0,0,0);
    Vector3 m_currentCollisionDirection = Vector3(0,0,0);
    float m_mass = 1;
    virtual void onStaticCollide(Vector3 normal);
    virtual void onTick(float nanos);
    virtual void onDraw(Graphics *g);
    virtual void onUI(Graphics *g);
    virtual void setPosition(Vector3 vector);
    virtual void onCollide (Entity* e) = 0;
    Vector3 boundingAABBSize = Vector3(0,0,0);
    Ellipsoid m_geometricEllipsoid = Ellipsoid(Vector3(0,0,0), Vector3(0,0,0), Vector3(0,0,0));
    Vector3 getPosition();
    Vector3 color = Vector3(1, 1, 1);
    Shape* getShape();
private:

protected:
    Shape *m_collisionShape;
    Vector3 m_position;
    World *m_world;

};

#endif // ENTITY_H
