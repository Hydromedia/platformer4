#include "graphics.h"


Graphics::Graphics(QGLWidget *v)
{
    m_textureNameToID = new QHash<QString, int>;
    m_atlasTileToCoordinate = new QHash<char, Vector2>;
    m_levelNumberToOBJ = new QHash<int, OBJ*>;
    m_view = v;
    //initializeGLFunctions(QGLContext::currentContext());
}

Graphics::~Graphics()
{
    delete m_textureNameToID;
    delete planes;
}
void Graphics::setUpScreenProjection(){
    GLint view2[4];
    glGetIntegerv(GL_VIEWPORT, view2);
    glMatrixMode(GL_PROJECTION); glPushMatrix();
    glLoadIdentity();
    // below is just one way to set up the view, see GL docs
    glOrtho(view2[0], view2[2], view2[1], view2[3], -1, 1);
    glMatrixMode(GL_MODELVIEW); glPushMatrix();
    glLoadIdentity();
}

void Graphics::undoScreenProjection(){
    glMatrixMode(GL_PROJECTION); glPopMatrix();
    glMatrixMode(GL_VIEWPORT); glPopMatrix();
}

void Graphics::drawQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
{
    glBegin(GL_QUADS);
    glVertex3f(v1.x, v1.y, v1.z);
    glVertex3f(v2.x, v2.y, v2.z);
    glVertex3f(v3.x, v3.y, v3.z);
    glVertex3f(v4.x, v4.y, v4.z);
    glEnd();
}

void Graphics::drawTexturedQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, QString name)
{
    if (m_textureNameToID->value(name)!=0){
        glBindTexture(GL_TEXTURE_2D, m_textureNameToID->value(name));
        glBegin(GL_QUADS);
        glTexCoord2f(0, 0); glVertex3f(v1.x, v1.y, v1.z);
        glTexCoord2f(0, 1); glVertex3f(v2.x, v2.y, v2.z);
        glTexCoord2f(1, 1); glVertex3f(v3.x, v3.y, v3.z);
        glTexCoord2f(1, 0); glVertex3f(v4.x, v4.y, v4.z);
        glEnd();
        glBindTexture(GL_TEXTURE_2D, 0); // always unbind textures when youre done
    }

}

void Graphics::loadTexture(QString path, QString name)
{
    QImage *image = new QImage(path); // load the image into the program (
    //QImage does some hard work for us, be thankful)
    *image = image->mirrored(false, true); // GL stores images differently than QT, so flip
    //some pixels
    int id = m_view->bindTexture(*image, GL_TEXTURE_2D, GL_RGBA, QGLContext::MipmapBindOption);
    //View extends from QGLWidget, which has convenience methods for binding textures.
    m_textureNameToID->insert(name, id);
    delete image; // GL stores a copy of the image, so unload it!

}

void Graphics::drawCylinder(Vector3 translation, float topRadius, float botRadius, float height, int slices, int stacks){
    glPushMatrix();
    glTranslatef(translation.x, translation.y, translation.z);
    GLUquadric *quadObj = gluNewQuadric();
    Vector3 vec = Vector3(-1,1,1);
    vec.normalize();
    glRotatef(120, vec.x, vec.y, vec.z);
    gluCylinder(quadObj, topRadius, botRadius, height, slices, stacks);
    glPopMatrix();
}

void Graphics::bindTexture(QString name)
{
    glBindTexture(GL_TEXTURE_2D, m_textureNameToID->value(name));
}

void Graphics::unbindTexture()
{
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Graphics::drawQuadWithBoundAtlasTexture(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, char c)
{
    glBegin(GL_QUADS);
    Vector2 index = m_atlasTileToCoordinate->value(c);
    glTexCoord2f(index.x, index.y);                                             glVertex3f(v1.x, v1.y, v1.z);
    glTexCoord2f(index.x, index.y + m_atlasIndexAmount);                        glVertex3f(v2.x, v2.y, v2.z);
    glTexCoord2f(index.x + m_atlasIndexAmount, index.y + m_atlasIndexAmount);   glVertex3f(v3.x, v3.y, v3.z);
    glTexCoord2f(index.x + m_atlasIndexAmount, index.y);                        glVertex3f(v4.x, v4.y, v4.z);
    glEnd();
}

void Graphics::drawRectangularPrismWithBoundAtlasTexture(float left, bool isLeft, float right, bool isRight,
                                                         float front, bool isFront, float back, bool isBack,
                                                         float top, bool isTop, float bottom, bool isBottom, char c)
{
    glBegin(GL_QUADS);
    Vector2 index = m_atlasTileToCoordinate->value(c);

    if (isLeft){
        //left
        //glColor3f(1, 0, 0);
        glTexCoord2f(index.x, index.y);                                             glVertex3f(left, top, back);
        glTexCoord2f(index.x, index.y + m_atlasIndexAmount);                        glVertex3f(left, bottom, back);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y + m_atlasIndexAmount);   glVertex3f(left, bottom, front);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y);                        glVertex3f(left, top, front);
        //std::cout << "index.x: " << index.x + m_atlasIndexAmount << ", index.y: " << index.y << std::flush;
    }

    if (isRight){
        //glColor3f(.5, 0, 0);
        //right
        glTexCoord2f(index.x, index.y);                                             glVertex3f(right, top, front);
        glTexCoord2f(index.x, index.y + m_atlasIndexAmount);                        glVertex3f(right, bottom, front);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y + m_atlasIndexAmount);   glVertex3f(right, bottom, back);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y);                        glVertex3f(right, top, back);
    }
    if (isFront){
        //glColor3f(0, 1, 0);
        //front
        glTexCoord2f(index.x, index.y);                                             glVertex3f(left, top, front);
        glTexCoord2f(index.x, index.y + m_atlasIndexAmount);                        glVertex3f(left, bottom, front);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y + m_atlasIndexAmount);   glVertex3f(right, bottom, front);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y);                        glVertex3f(right, top, front);
    }
    if (isBack)
    {
        //glColor3f(0, .5, 0);
        //back
        glTexCoord2f(index.x, index.y);                                             glVertex3f(right, top, back);
        glTexCoord2f(index.x, index.y + m_atlasIndexAmount);                        glVertex3f(right, bottom, back);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y + m_atlasIndexAmount);   glVertex3f(left, bottom, back);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y);                        glVertex3f(left, top, back);
    }
    if (isTop)
    {
        //glColor3f(0, 0, 1);
        //top
        glTexCoord2f(index.x, index.y);                                             glVertex3f(left, top, back);
        glTexCoord2f(index.x, index.y + m_atlasIndexAmount);                        glVertex3f(left, top, front);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y + m_atlasIndexAmount);   glVertex3f(right, top, front);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y);                        glVertex3f(right, top, back);
    }
    if (isBottom)
    {
        //glColor3f(0, 0, .5);
        //bottom
        glTexCoord2f(index.x, index.y);                                             glVertex3f(right, bottom, back);
        glTexCoord2f(index.x, index.y + m_atlasIndexAmount);                        glVertex3f(right, bottom, front);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y + m_atlasIndexAmount);   glVertex3f(left, bottom, front);
        glTexCoord2f(index.x + m_atlasIndexAmount, index.y);                        glVertex3f(left, bottom, back);
    }

    glEnd();
}

void Graphics::updateView()
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();


    float *projection;
    projection = new float[16];
    glGetFloatv(GL_PROJECTION_MATRIX, projection);
    //std::cout << "Projection\n";
    //debugArray(projection, 16);


    float *view;
    view = new float[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, view);
    //std::cout << "View\n";
    //debugArray(view, 16);

    glLoadIdentity();
    glMultMatrixf(projection);
    glMultMatrixf(view);

    float *res;
    res = new float[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, res);

    //std::cout << "Res\n";
    //debugArray(res, 16);

    Vector4 r0 = Vector4(res[0], res[4], res[8], res[12]);
    Vector4 r1 = Vector4(res[1], res[5], res[9], res[13]);
    Vector4 r2 = Vector4(res[2], res[6], res[10], res[14]);
    Vector4 r3 = Vector4(res[3], res[7], res[11], res[15]);
    glPopMatrix();
    updateViewPlanes(r3-r0, r3+r0, r3-r1, r3+r1, r3-r2, r3+r2);
    //std::cout << r0 << r1 << r2  << r3 << std::flush;
}

void Graphics::updateViewPlanes(Vector4 negativeX, Vector4 positiveX, Vector4 negativeY, Vector4 positiveY, Vector4 negativeZ, Vector4 positiveZ)
{
    negX =negativeX;
    posX= positiveX;
    negY= negativeY;
    posY= positiveY;
    negZ= negativeZ;
    posZ= positiveZ;
    planes = new QList<Vector4>();
    planes->append(negX);
    planes->append(posX);
    planes->append(negY);
    planes->append(posY);
    planes->append(negZ);
    planes->append(posZ);
}

void Graphics::initializeGL()
{

}

void Graphics::readObj(QString s, int level)
{
    m_levelNumberToOBJ->value(level)->read(s);
}

void Graphics::drawObj(int level, float yOffset)
{
    m_levelNumberToOBJ->value(level)->draw(yOffset);
}

void Graphics::addToLevelObj(int level, OBJ *o)
{
    m_levelNumberToOBJ->insert(level, o);
}

void Graphics::addToTileCoordinateMap(char c, Vector2 vec)
{
    m_atlasTileToCoordinate->insert(c, vec);
}
