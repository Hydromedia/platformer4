#ifndef BLOCK
#define BLOCK

struct Block {
    bool isPassable;
    bool topShown;
    bool bottomShown;
    bool leftShown;
    bool rightShown;
    bool frontShown;
    bool backShown;
    char type;
    Block(){
        isPassable = true;
        topShown = false;
        bottomShown = false;
        leftShown = false;
        rightShown = false;
        frontShown = false;
        backShown = false;
        type = 1;
    }
} __attribute((packed));

#endif // BLOCK

