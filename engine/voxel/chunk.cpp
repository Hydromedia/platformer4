#include "chunk.h"
#include <QGLFunctions>

Chunk::Chunk(char size, Vector3 position)
{
    chunkSize = size;
    blocks = new Block[size*size*size];
    m_position = position;
    //four vertices, 6 faces per vertex
}

Chunk::~Chunk()
{
    delete blocks;
    delete bufferData;
}

void Chunk::resetVBO(Graphics *g)
{
    int numVerts = 0;
    bufferData = new float[chunkSize*chunkSize*chunkSize*120];
    for (int i = 0; i < chunkSize; i++) {
        for (int j = 0; j < chunkSize; j++) {
            for (int u = 0; u < chunkSize; u++) {
                if (blocks[i+ chunkSize * (j + chunkSize * u)].type!=1 ){
                    if (blocks[i+ chunkSize * (j + chunkSize * u)].topShown) {
                        addFace(g, blocks[i+ chunkSize * (j + chunkSize * u)].type,
                                numVerts, Vector3(i, j+1, u), Vector3(i+1, j+1, u), Vector3(i+1, j+1, u+1), Vector3(i, j+1, u+1));
                        //3 floats per vertex, 2 floats per tex coord, 4 vertices
                        numVerts += 3*4 + 2*4;
                    } if (blocks[i+ chunkSize * (j + chunkSize * u)].bottomShown) {
                        addFace(g, blocks[i+ chunkSize * (j + chunkSize * u)].type,
                                numVerts, Vector3(i, j, u+1), Vector3(i+1, j, u+1), Vector3(i+1, j, u), Vector3(i, j, u));
                        //3 floats per vertex, 2 floats per tex coord, 4 vertices
                        numVerts += 3*4 + 2*4;
                    } if (blocks[i+ chunkSize * (j + chunkSize * u)].leftShown) {
                        addFace(g, blocks[i+ chunkSize * (j + chunkSize * u)].type,
                                numVerts, Vector3(i, j, u), Vector3(i, j+1, u), Vector3(i, j+1, u+1), Vector3(i, j, u+1));
                        //3 floats per vertex, 2 floats per tex coord, 4 vertices
                        numVerts += 3*4 + 2*4;
                    } if (blocks[i+ chunkSize * (j + chunkSize * u)].rightShown) {
                        addFace(g,  blocks[i+ chunkSize * (j + chunkSize * u)].type,
                                numVerts, Vector3(i+1, j, u+1), Vector3(i+1, j+1, u+1), Vector3(i+1, j+1, u), Vector3(i+1, j, u));
                        //3 floats per vertex, 2 floats per tex coord, 4 vertices
                        numVerts += 3*4 + 2*4;
                    } if (blocks[i+ chunkSize * (j + chunkSize * u)].backShown) {
                        addFace(g, blocks[i+ chunkSize * (j + chunkSize * u)].type,
                                numVerts, Vector3(i+1, j, u), Vector3(i+1, j+1, u), Vector3(i, j+1, u), Vector3(i, j, u));
                        //3 floats per vertex, 2 floats per tex coord, 4 vertices
                        numVerts += 3*4 + 2*4;
                    } if (blocks[i+ chunkSize * (j + chunkSize * u)].frontShown) {
                        addFace(g, blocks[i+ chunkSize * (j + chunkSize * u)].type,
                                numVerts, Vector3(i, j, u+1), Vector3(i, j+1, u+1), Vector3(i+1, j+1, u+1), Vector3(i+1, j, u+1));
                        //3 floats per vertex, 2 floats per tex coord, 4 vertices
                        numVerts += 3*4 + 2*4;
                    }
                }
            }
        }
    }

    g->glGenBuffers(1, &bufferID); // Generate a new id
    g->glBindBuffer(GL_ARRAY_BUFFER, bufferID); // Bind the buffer
    // Upload the data. GL_STATIC_DRAW is a hint that the buffer
    // contents will be modified once but used many times.
    g->glBufferData(GL_ARRAY_BUFFER, sizeof(float)*numVerts, bufferData, GL_STATIC_DRAW);
    g->glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind the buffer
    delete bufferData;
    m_numVerts = numVerts;
}

void Chunk::onDraw(Graphics *g)
{
    // Rendering (interleaved data format)
    g->glBindBuffer(GL_ARRAY_BUFFER, bufferID);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    unsigned int stride = sizeof(float) * (3 + 2); // Spacing between vertices
    glVertexPointer(3, GL_FLOAT, stride, (char *) 0);
    glTexCoordPointer(2, GL_FLOAT, stride, (char *) (3 * sizeof(float)));
    glDrawArrays(GL_QUADS, 0, (m_numVerts)/5);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    g->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Chunk::addFace(Graphics *g, char texture, int numVerts, Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4){
    Vector2 index = g->m_atlasTileToCoordinate->value(texture);
    float indexAmount = g->m_atlasIndexAmount;
    bufferData[numVerts] =v1.x+m_position.x*chunkSize;
    bufferData[numVerts+1] = v1.y+m_position.y*chunkSize;
    bufferData[numVerts+2] = v1.z+m_position.z*chunkSize;
    bufferData[numVerts + 3] = index.x;
    bufferData[numVerts + 4] = index.y;
    numVerts+= 5;

    bufferData[numVerts] =v2.x+m_position.x*chunkSize;
    bufferData[numVerts+1] = v2.y+m_position.y*chunkSize;
    bufferData[numVerts+2] = v2.z+m_position.z*chunkSize;
    bufferData[numVerts + 3] = index.x;
    bufferData[numVerts + 4] = index.y + indexAmount;
    numVerts+= 5;

    bufferData[numVerts] =v3.x+m_position.x*chunkSize;
    bufferData[numVerts+1] = v3.y+m_position.y*chunkSize;
    bufferData[numVerts+2] = v3.z+m_position.z*chunkSize;
    bufferData[numVerts + 3] = index.x + indexAmount;
    bufferData[numVerts + 4] = index.y + indexAmount;
    numVerts+= 5;

    bufferData[numVerts] =v4.x+m_position.x*chunkSize;
    bufferData[numVerts+1] = v4.y+m_position.y*chunkSize;
    bufferData[numVerts+2] = v4.z+m_position.z*chunkSize;
    bufferData[numVerts + 3] = index.x + indexAmount;
    bufferData[numVerts + 4] = index.y;
    numVerts+= 5;
}

