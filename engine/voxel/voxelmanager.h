#ifndef VOXELMANAGER_H
#define VOXELMANAGER_H
#include "engine/core/graphics.h"
#include "chunk.h"
#include <QHash>
#include <QPair>
#include "engine/core/manager.h"
#include "engine/core/entity.h"
#include "block.h"

class VoxelManager : public Manager
{
public:
    VoxelManager(Graphics *g, World *w);
    ~VoxelManager();

    Vector3 collide(Entity *e, Vector3 oldPosition, Vector3 potentialPosition);
    virtual void onTick(long seconds);
    virtual void onDraw(Graphics *g) = 0;
    virtual void generateTerrain();
    virtual void initializeBlocks(Chunk*) = 0;
    void resetVBO();
    void initializeWorld(int worldSX, int worldSY, int worldSZ, int chunkS);
    Block getBlock(int x, int y, int z);
protected:
    int worldSizeX;
    int worldSizeY;
    int worldSizeZ;
    int chunkSize;
    Chunk* getChunk(int x, int y, int z);
    void addChunk(Chunk* c, int x, int y, int z);
    void initializeChunks();
    QHash< QPair<int, QPair<int, int> >, Chunk* > chunks;
private:
    void xSweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition);
    void ySweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition);
    void zSweep(Entity *e, Vector3 *oldPosition, Vector3 *potentialPosition);

};

#endif // VOXELMANAGER_H
