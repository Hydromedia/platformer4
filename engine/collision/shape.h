#ifndef SHAPE_H
#define SHAPE_H
#include "engine/core/entity.h"


class Entity;
class Cylinder;
class Shape
{
public:
    Shape(Entity *e);
    ~Shape();
    virtual Vector3 collides(Shape *s) = 0;
    virtual Vector3 collidesCylinder(Cylinder *c) = 0;
    virtual void onDraw(Graphics *g) = 0;

protected:
    Entity *m_entity;
};

#endif // SHAPE_H
