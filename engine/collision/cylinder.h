#ifndef CYLINDER_H
#define CYLINDER_H
#include "engine/collision/shape.h"
#include "engine/core/entity.h"



class Cylinder : public Shape
{
public:
    Cylinder(Entity *e, float radius, float height);
    ~Cylinder();
    Vector3 collides(Shape *s) override;
    Vector3 collidesCylinder(Cylinder *c) override;
    void onDraw(Graphics *g) override;

    float getRadius();
    void setRadius(float rad);

    float getHeight();
    void setHeight(float h);

private:
    float m_radius;
    float m_height;
};

#endif // CYLINDER_H
