#ifndef ELLIPSOID_H
#define ELLIPSOID_H
#include "engine/common/ray.h"


class Ellipsoid
{
public:
    Vector3 start = Vector3(0,0,0);
    Vector3 end = Vector3(0,0,0);
    Ellipsoid(Vector3 star, Vector3 en, Vector3 rad);
    ~Ellipsoid();
    Vector3 radius;
    CollisionInfo ellipsoidTriangle(Triangle *tr);
    CollisionInfo ellipsoidEdge(Vector3 C, Vector3 D);
    CollisionInfo ellipsoidVertex(Vector3 vert);
private:

};

#endif // ELLIPSOID_H
