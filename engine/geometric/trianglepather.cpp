#include "trianglepather.h"
#include <cfloat>
#include <algorithm>

TrianglePather::TrianglePather(Graphics *g, World *w, Vector3 start, Vector3 goal, Ellipsoid *ellipse, QList<Triangle*> *tris, int level)
{
    m_level = level;
    worldTriangles = tris;
    m_world = w;
    m_graphics = g;
    goalEllipsoid = ellipse;
    start = start;
    goal = goal;
    loadNavMesh();
    initializeGraph();
//    loadNavMesh();
//    std::cout << "CONSTRUCTOR" <
//    initializeGraph();
}

TrianglePather::~TrianglePather()
{

}

void TrianglePather::setStart(Vector3 start)
{
    m_start = start;
}

void TrianglePather::setStartAndGoal(Vector3 start, Vector3 goal){
    m_start = start;
    m_goal = goal;
}

void TrianglePather::setStartAndGoalDirection(Vector3 start, Vector3 goal)
{
    m_start = start;

    float prevDist = FLT_MAX;

    Ray goalRay1 = Ray(start, goal);
    CollisionInfo c2 = goalRay1.rayEllipsoidFromEllipsoidSpace(goalEllipsoid);
    //std::cout << c2.collisionPoint*goalEllipsoid->radius << ", t: " << c2.t << std::endl;
    float dist = (c2.collisionPoint-start).lengthSquared();
    if (c2.t >= 0 && dist < prevDist) {
        prevDist = dist;
        m_goal = c2.collisionPoint;
        m_drawEnd = c2.collisionPoint;

    }

    Ray goalRay2 = Ray(start, goal);
    for (int i = 0; i < worldTriangles->size(); i++) {
        CollisionInfo c1 = goalRay2.rayTriangle(worldTriangles->at(i));
        float dist = (c1.collisionPoint-start).lengthSquared();
        if (c1.collided && c1.t >= 0 && dist < prevDist) {
            prevDist = dist;
            m_goal = c1.collisionPoint+worldTriangles->at(i)->normal;
            m_drawEnd = c1.collisionPoint+worldTriangles->at(i)->normal;
        }
    }
    goalEllipsoid->start = m_drawEnd;
    goalEllipsoid->end = m_drawEnd;
}

void TrianglePather::generatePath()
{
    currentPath = QList<Vector3>();



    NavigationTriangle *end = findPathTriangles();

    QList<NavigationTriangle*> portalTriangles = QList<NavigationTriangle*>();
    QList<Edge> portals = QList<Edge>();

    NavigationTriangle* curr = end;
    if (curr == 0) {
        return;
    }
    portals.push_front(Edge(m_goal, m_goal));
    while (curr->prev != 0) {
        portals.push_front(curr->edgeToPrev);
        portalTriangles.push_front(curr);
        curr = curr->prev;
    }
    portalTriangles.push_front(curr);
    currentPath.append(m_start);
    Vector3 apex = m_start;
    int leftIndex = 0;
    int rightIndex = 0;
    Vector3 leftSide = portalTriangles.at(0)->getLeftVertex(portals.at(0))*.99 + portalTriangles.at(0)->getRightVertex(portals.at(0))*.01;
    Vector3 rightSide = portalTriangles.at(0)->getLeftVertex(portals.at(0))*.01 + portalTriangles.at(0)->getRightVertex(portals.at(0))*.99;

    NavigationTriangle currLeft = *portalTriangles.at(0);
    NavigationTriangle currRight = *portalTriangles.at(0);

    for (int i = 0; i < portals.length(); i++) {
        //std::cout << "Tri number: " << i <<std::endl;
        //try to move left point to left vertex of left portal

            NavigationTriangle newLeft = *portalTriangles.at(i);
            Vector3 newLeftSide = newLeft.getLeftVertex(portals.at(i))*.99 + newLeft.getRightVertex(portals.at(i))*.01;
            //currentLeft.append(newLeftSide);
            //if inside the funnel, narrow the funnel
            if (((rightSide - apex).cross(newLeftSide - apex)).dot((Vector3(0,1,0))) < 0 && leftSide != apex) {
                //crossed over
                //std::cout << "Left cross" << std::endl;
                currentPath.append(rightSide);
                i = leftIndex;
                rightIndex = leftIndex;
                apex = rightSide;
                leftSide = rightSide;
                currRight = *portalTriangles.at(i);
                currLeft = *portalTriangles.at(i);
                continue;
            } else if (((leftSide - apex).cross(newLeftSide-apex)).dot((Vector3(0,1,0))) < 0 || leftSide == apex) {
                //narrower
                //std::cout << "Left narrow" << std::endl;
                rightIndex = i;
                currLeft = *portalTriangles.at(i);
                leftSide = currLeft.getLeftVertex(portals.at(i))*.99 + currLeft.getRightVertex(portals.at(i))*.01;
            } else {
                //wider
                //do nothing
            }

        //try to move right point to right vetex of next portal (analogous)
            //if inside the funnel, narrow the funnel
            NavigationTriangle newRight = *portalTriangles.at(i);
            Vector3 newRightSide = newRight.getLeftVertex(portals.at(i))*.01 + newRight.getRightVertex(portals.at(i))*.99;
            //currentRight.append(newRightSide);
            if (((leftSide - apex).cross(newRightSide - apex)).dot((Vector3(0,1,0))) > 0 && rightSide != apex) {
                //crossed over
                //std::cout << "Right cross" << std::endl;
                currentPath.append(leftSide);
                i = rightIndex;
                leftIndex = rightIndex;
                apex = leftSide;
                rightSide = leftSide;
                currLeft = *portalTriangles.at(i);
                currRight = *portalTriangles.at(i);
                continue;
            } else if (((rightSide - apex).cross(newRightSide-apex)).dot((Vector3(0,1,0))) > 0 || rightSide == apex) {
                //narrower
                //std::cout << "Right narrow" << "Right Side: " << rightSide << "New Right Side: " << newRightSide << std::endl;
                leftIndex = i;
                currRight = *portalTriangles.at(i);
                rightSide = currRight.getLeftVertex(portals.at(i))*.01 + currRight.getRightVertex(portals.at(i))*.99;
            } else {
                //wider
                //do nothing
            }

            if (i == portals.length()-1) {
                //std::cout << "End path" << std::endl;
                currentPath.append(newRightSide);
            }
        }


    //std::cout << "Pathfinding done, Portals: " << portals.length() - 1 << " Path Size: " << currentPath.length() << std::endl;
    //std::cout << "Triangles: " << portalTriangles.length() << std::endl;
//    for (int i = 0; i < currentPath.length(); i++){
//        std::cout << currentPath.at(i) << std::endl;
//    }
}

NavigationTriangle* TrianglePather::findPathTriangles()
{
    pathTriangles = QList<NavigationTriangle*>();
    NavigationTriangle *start = new NavigationTriangle(Vector3(0,0,0),Vector3(0,0,0),Vector3(0,0,0), 0, 0, 0);
    NavigationTriangle *end = new NavigationTriangle(Vector3(0,0,0),Vector3(0,0,0),Vector3(0,0,0), 0, 0, 0);
    float prevStart = FLT_MAX;
    float prevEnd = FLT_MAX;
    for (int i = 0; i < meshTriangles.size(); i++) {
        Ray startRay = Ray(m_start + Vector3(0, 0, 0), Vector3(0,-1,0));
        CollisionInfo c1 = startRay.rayTriangle(meshTriangles.at(i));
        if (c1.collided && c1.t >= 0 && c1.t < prevStart) {
            prevStart = c1.t;
            start = meshTriangles.at(i);
        }
        Ray endRay = Ray(m_goal, Vector3(0,-1,0));
        CollisionInfo c2 = endRay.rayTriangle(meshTriangles.at(i));
        if (c2.collided && c2.t >= 0 && c2.t < prevEnd) {
            prevEnd = c2.t;
            end = meshTriangles.at(i);
        }
    }

    return generateGraph(start, end);

}
void TrianglePather::initializeGraph(){
    std::cout << meshTriangles.size() << std:: endl;
    for (int i = 0; i < meshTriangles.size(); i++){
        NavigationTriangle *t = meshTriangles.at(i);
        QPair<int, int> key1 = QPair<int, int>(std::min(t->a,t->b), std::max(t->a,t->b));
        edges[key1] += t;

        QPair<int, int> key2 = QPair<int, int>(std::min(t->a,t->c), std::max(t->a,t->c));
        edges[key2] += t;

        QPair<int, int> key3 = QPair<int, int>(std::min(t->b,t->c), std::max(t->b,t->c));
        edges[key3] += t;
    }
}

NavigationTriangle* TrianglePather::generateGraph(NavigationTriangle *start, NavigationTriangle *end){

    //initializeGraph();
    for (int i = 0; i < meshTriangles.length(); i++){
        NavigationTriangle *tri = meshTriangles.at(i);
            tri->discovered = false;
            tri->prev = 0;
    }

    QList<NavigationTriangle*> searchQueue = QList<NavigationTriangle*>();
    searchQueue.push_back(start);
    start->discovered = true;
    while (searchQueue.isEmpty() == false){
        NavigationTriangle *vertex = searchQueue.front();
        searchQueue.pop_front();
        for (int i = 0; i < edges.value(QPair<int, int>(std::min(vertex->a, vertex->b), std::max(vertex->a, vertex->b))).length(); i++){
            NavigationTriangle *tri = edges.value(QPair<int, int>(std::min(vertex->a, vertex->b), std::max(vertex->a, vertex->b))).at(i);
            if (tri->discovered == false){
                tri->discovered = true;
                tri->prev = vertex;
                searchQueue.push_back(tri);
                tri->edgeToPrev = Edge(vertex->vertices[0], vertex->vertices[1]);

            }
        }
        for (int i = 0; i < edges.value(QPair<int, int>(std::min(vertex->a, vertex->c), std::max(vertex->a, vertex->c))).length(); i++){
            NavigationTriangle *tri = edges.value(QPair<int, int>(std::min(vertex->a, vertex->c), std::max(vertex->a, vertex->c))).at(i);
            if (tri->discovered == false){
                tri->discovered = true;
                tri->prev = vertex;
                searchQueue.push_back(tri);
                tri->edgeToPrev = Edge(vertex->vertices[0], vertex->vertices[2]);
            }
        }
        for (int i = 0; i < edges.value(QPair<int, int>(std::min(vertex->b, vertex->c), std::max(vertex->b, vertex->c))).length(); i++){
            NavigationTriangle *tri = edges.value(QPair<int, int>(std::min(vertex->b, vertex->c), std::max(vertex->b, vertex->c))).at(i);
            if (tri->discovered == false){
                tri->discovered = true;
                tri->prev = vertex;
                searchQueue.push_back(tri);
                tri->edgeToPrev = Edge(vertex->vertices[1], vertex->vertices[2]);
            }
        }
    }


    //std::cout << "Path triangles: "  << pathTriangles.length() << std::endl;
    if (end->prev == 0 && end != start) {
        std::cout << "Return 0" << std::endl;
        return 0;

    } else {
        //pathTriangles is used for drawing
        NavigationTriangle* curr = end;
        pathTriangles.append(curr);
        while (curr->prev != 0) {
            curr = curr->prev;
            pathTriangles.append(curr);
        }
        return end;
    }
}

void TrianglePather::onDraw(Graphics *g)
{

}

QList<Vector3> TrianglePather::getPath()
{
    return currentPath;
}

void TrianglePather::loadNavMesh()
{
    for (int i = 0; i< m_graphics->m_levelNumberToOBJ->value(0-m_level)->triangles.size(); i++){
        Vector3 x = m_graphics->m_levelNumberToOBJ->value(0-m_level)->vertices.at(m_graphics->m_levelNumberToOBJ->value(0-m_level)->triangles.at(i).a.vertex);
        Vector3 y = m_graphics->m_levelNumberToOBJ->value(0-m_level)->vertices.at(m_graphics->m_levelNumberToOBJ->value(0-m_level)->triangles.at(i).b.vertex);
        Vector3 z = m_graphics->m_levelNumberToOBJ->value(0-m_level)->vertices.at(m_graphics->m_levelNumberToOBJ->value(0-m_level)->triangles.at(i).c.vertex);
        NavigationTriangle *tri = new NavigationTriangle(x, y, z,
                                                         m_graphics->m_levelNumberToOBJ->value(0-m_level)->triangles.at(i).a.vertex,
                                                         m_graphics->m_levelNumberToOBJ->value(0-m_level)->triangles.at(i).b.vertex,
                                                         m_graphics->m_levelNumberToOBJ->value(0-m_level)->triangles.at(i).c.vertex);
        meshTriangles.push_front(tri);
    }
    std::cout << meshTriangles.size()  << "size" << std::endl;
}

