#include "geometricmanager.h"

GeometricManager::GeometricManager(Graphics *g, World *w) : Manager (g, w)
{
    loadTerrain();
}

GeometricManager::~GeometricManager()
{

}

void GeometricManager::onTick(long seconds)
{

}

void GeometricManager::loadTerrain() {

}



Vector3 GeometricManager::collide(Entity *e, Vector3 oldPosition, Vector3 potentialPosition)
{
    int iterations = 5;
    bool collided = true;
    Vector3 result = potentialPosition;
    Vector3 direction;
    CollisionInfo info;
    while (iterations > 0 && collided){
        //CollisionInfo info;
        info.t = 1;
        for (int i = 0; i < triangles.size(); i++) {
            Ellipsoid el = Ellipsoid(oldPosition, potentialPosition, e->m_geometricEllipsoid.radius);
            CollisionInfo retInfo = el.ellipsoidTriangle(triangles.at(i));
            if (retInfo.t < info.t && retInfo.t >=0) {
                info = retInfo;
            }
        }

        if (info.t <= 1 && info.t >=0){
            Vector3 A = oldPosition;
            Vector3 B = potentialPosition;
            Vector3 hit = A + (B-A)*info.t;

            if (!(info.normal.x == 0 && info.normal.y == 0 && info.normal.z == 0) &&
                  info.normal.x == info.normal.x && info.normal.y == info.normal.y && info.normal.z == info.normal.z) {


                currNormal = info.normal;
                entityPos = e->getPosition();

                info.normal.normalize();
                Vector3 C = (B-A)*(1-info.t);
                //Vector3 D = C - (C.dot(info.normal))*info.normal;
                direction = C - ((info.normal.dot(C))/info.normal.dot(Vector3(0,1,0)))*(Vector3(0,1,0));
                float const length = (C -(info.normal.dot(C))*info.normal).length();
                if (!(direction.x == 0 && direction.y == 0 && direction.z == 0) &&
                      direction.x == direction.x && direction.y == direction.y && direction.z == direction.z) {
                    direction.normalize();
                } else {
                    direction = Vector3(0,0,0);
                }

                //std::cout << "Direction" << direction << std::endl;
                //std::cout << "Iterations Left: " << iterations << std::endl;


                if (info.normal.dot(Vector3(0,1,0)) == 0) {
                    e->m_velocity = e->m_velocity - e->m_velocity.dot(info.normal)*info.normal;
//                    Vector3 direction = C - (info.normal.dot(C))/info.normal;
//                    float const length = (C -(info.normal.dot(C))*info.normal).length();
                    Vector3 final = hit+C-(info.normal.dot(C))*info.normal;
                    result = final + info.normal*.0001;
                    oldPosition = hit + info.normal*.0001;
                    potentialPosition = result;
                } else {
                    e->m_velocity = e->m_velocity - e->m_velocity.dot(info.normal*(Vector3(0,1,0)))*(info.normal*(Vector3(0,1,0)));
                    Vector3 final = hit+direction*length;
                    result = final + info.normal*.0001;
                    oldPosition = hit + info.normal*.0001;
                    potentialPosition = result;
                }
                //std::cout <<"Normal: " << info.normal << std::endl;
                e->onStaticCollide(info.normal);
                collided = true;
            } else {
                collided = false;
            }
        } else {
            collided = false;
        }
        iterations--;
    }
    //std::cout <<"Normal: " << info.normal << std::endl;
    //std::cout << "Direction" << direction << std::endl;
    //std::cout << "" << << std::endl;
    //std::cout << "" << << std::endl;
    return result;
}

