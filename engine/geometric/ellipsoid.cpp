#include "ellipsoid.h"
#include <cmath>
#include <qgl.h>
#include <GL/glu.h>

Ellipsoid::Ellipsoid(Vector3 star, Vector3 en, Vector3 rad)
{
    start = star;
    end = en;
    radius = rad;
}

Ellipsoid::~Ellipsoid()
{

}

CollisionInfo Ellipsoid::ellipsoidTriangle(Triangle *tr)
{
    CollisionInfo info;
    info.normal = tr->normal;
    info.t = 1;
    Vector3 A = start/radius;
    Vector3 B = end/radius;
    Triangle tri = *tr;
    //bool gotToPlane = false;

    for (int j = 0; j < 3; j++){
        tri.vertices[j] = tri.vertices[j]/radius;
    }

    tri.normal = (tri.vertices[1] - tri.vertices[0]).cross(tri.vertices[2] - tri.vertices[0]);
    //tri.normal = tri.normal/radius;
    tri.normal.normalize();
    Vector3 N = tri.normal;
    Vector3 S = tri.vertices[0];

    float newT = (-1)*((N.dot(A-N-S))/(N.dot(B-A)));
    Vector3 P = (A - N) + (B-A)*newT;
    Vector3 dir = (B-A);
    Ray ray = Ray(A-N, dir);
    CollisionInfo retInfo = ray.rayTriangle(&tri);

//    if (retInfo.t < info.t && retInfo.t >= 0){
//        std::cout << "Plane? " << retInfo.t << std::endl;
//        std::cout << "True1" << std::endl;
//        /*if (retInfo.t >= 0){
//            std::cout << "True2" << std::endl;
//        }*/ if (retInfo.collided) {
//            std::cout << "True3" << std::endl;
//        } if (tri.normal.dot(B - A) < 0) {
//             //triangle.n.dot(B - A) < 0
//            std::cout << "True4" << std::endl;
//        }
//    }
    if (retInfo.t < info.t && retInfo.t >= 0 && retInfo.collided && tri.normal.dot(B - A) < 0){
        info.t = retInfo.t;
        info.collisionPoint = radius*retInfo.collisionPoint;
//        tri.normal = (tri.vertices[1] - tri.vertices[0]).cross(tri.vertices[2] - tri.vertices[0]);
//        info.normal = tri.normal;
        //std::cout << "Triangle:" << tr << std::endl;
        //std::cout << "Plane: " << "T: " << info.t << " Tri: " << tr << std::endl;
        //gotToPlane = true;
    } else {
        //std::cout << "Edge?" << std::endl;
        for (int j = 0; j < 3; j++){
             Vector3 C = tri.vertices[j];
             Vector3 D;
             if (j == 2){
                 D = tri.vertices[0];
             } else {
                 D = tri.vertices[j+1];
             }
             CollisionInfo retInfo = ellipsoidEdge(C, D);
            if (retInfo.t < info.t && retInfo.t >= 0){
                info.t = retInfo.t;
                info.collisionPoint = retInfo.collisionPoint*radius;
                Vector3 E = (start + (end-start)*info.t);
                info.normal = ((E-info.collisionPoint)/radius)/radius;
                //std::cout << "Got to edge: " << "T: " << info.t << " Tri: " << tr << std::endl;
            }
        }
        for (int j = 0; j < 3; j++) {
            CollisionInfo retInfo = ellipsoidVertex(tri.vertices[j]);
           if (retInfo.t < info.t && retInfo.t >= 0){
               //std::cout << "Got to vertex" << std::endl;
               info.t = retInfo.t;
               info.collisionPoint = retInfo.collisionPoint*radius;
               Vector3 E = (start + (end-start)*info.t);
               info.normal = ((E-info.collisionPoint)/radius)/radius;

           }
        }
    }
    return info;
}

CollisionInfo Ellipsoid::ellipsoidEdge(Vector3 C, Vector3 D)
{
    CollisionInfo info;

    Vector3 A = start/radius;
    Vector3 B = end/radius;

    float a = ((B-A).cross(D-C)).lengthSquared();
    float b = 2*((B-A).cross(D-C)).dot((A-C).cross(D-C));
    float c = (((A-C).cross(D-C)).lengthSquared()) - (D-C).lengthSquared();
    float newT1 = (-b + std::sqrt(b*b-4*a*c))/(2*a);
    float newT2 = (-b - std::sqrt(b*b-4*a*c))/(2*a);
    float newT = 1;
    if (newT1 > 0 && newT1 <= newT2) {
        newT = newT1;
    } else if (newT2 > 0 && newT2 <= newT1) {
        newT = newT2;
    } else {
        //std::cout << "They were both negative?" << std::endl;
    }

    Vector3 P = A+(B-A)*newT;
    //intersection is

    if (0 < (P-C).dot(D-C) && (P-C).dot(D-C) < (D-C).lengthSquared()
        && newT < info.t && newT >= 0){
        info.t = newT;

        Vector3 DC = D - C;
        DC.normalize();
        Vector3 PC = P-C;
        PC.normalize();
        info.collisionPoint = (P-C).dot(DC)*(DC)+C;
        Vector3 X = info.collisionPoint;
    }
    return info;
}

CollisionInfo Ellipsoid::ellipsoidVertex(Vector3 vert)
{
    CollisionInfo info;
    Vector3 A = start/radius;
    Vector3 B = end/radius;

    Vector3 dir = (vert-(B-A));
    Ray ray = Ray(vert, dir);
    CollisionInfo retInfo = ray.rayEllipsoid(this);
    if (retInfo.t < info.t && retInfo.t >= 0){
        info.t = retInfo.t;
        info.normal = retInfo.normal;
        info.collisionPoint = retInfo.collisionPoint;
    }
    return info;
}

