#include "navigationtriangle.h"

NavigationTriangle::NavigationTriangle(Vector3 v1, Vector3 v2, Vector3 v3, int first, int second, int third) : Triangle (v1, v2, v3)
{
    a = first;
    b = second;
    c = third;

}

NavigationTriangle::~NavigationTriangle()
{

}

Vector3 NavigationTriangle::getLeftVertex(Edge e){
    Vector3 leftSide;
    if (((e.a - centroid()).cross(e.b - centroid())).dot((Vector3(0,1,0))) > 0) {
        leftSide = e.b;
    } else {
        leftSide = e.a;
    }
    return leftSide;

}

Vector3 NavigationTriangle::getRightVertex(Edge e){
    Vector3 rightSide;
    if (((e.a - centroid()).cross(e.b - centroid())).dot((Vector3(0,1,0))) > 0) {
        rightSide = e.a;
    } else {
        rightSide = e.b;
    }
    return rightSide;
}
