#ifndef EDGE_H
#define EDGE_H
#include "src/vector.h"

class Edge
{
public:
    Edge(Vector3 e1, Vector3 e2);
    ~Edge();
    Vector3 a;
    Vector3 b;
};

#endif // EDGE_H
