#ifndef TRIANGLEPATHER_H
#define TRIANGLEPATHER_H
#include "engine/common/ray.h"
#include "engine/core/graphics.h"
#include "engine/core/world.h"
#include "engine/geometric/triangle.h"
#include "engine/geometric/collisioninfo.h"
#include "engine/geometric/navigationtriangle.h"
#include <QList>

class TrianglePather
{
public:
    TrianglePather(Graphics *g, World *w, Vector3 start, Vector3 goal, Ellipsoid *ellipse, QList<Triangle*> *tris, int level);
    ~TrianglePather();

    QList<Triangle*> *worldTriangles;
    void setStart(Vector3 start);
    virtual void setStartAndGoalDirection(Vector3 start, Vector3 goal);
    virtual void setStartAndGoal(Vector3 start, Vector3 goal);
    void generatePath();
    QList<Vector3> getPath();


protected:
    void initializeGraph();
    NavigationTriangle* generateGraph(NavigationTriangle *start, NavigationTriangle *end);
    NavigationTriangle* findPathTriangles();
    virtual void loadNavMesh();
    virtual void onDraw(Graphics *g);
    int m_level;
    Ellipsoid *goalEllipsoid;
    Vector3 m_start = Vector3(1,1,1);
    Vector3 m_goal = Vector3(2,2,2);
    Vector3 m_drawEnd = Vector3(2,2,2);
    World *m_world;
    Graphics *m_graphics;
    QList<Vector3> currentPath = QList<Vector3>();
    QHash<QPair<int, int>, QList<NavigationTriangle *> > edges = QHash<QPair<int, int>, QList<NavigationTriangle *> >();
    QList<NavigationTriangle*> meshTriangles = QList<NavigationTriangle*>();
    QList<NavigationTriangle*> pathTriangles = QList<NavigationTriangle*>();


};

#endif // TRIANGLEPATHER_H
