#ifndef COLLISIONINFO
#define COLLISIONINFO
#include "src/vector.h"

struct CollisionInfo {
    Vector3 normal;
    Vector3 collisionPoint;
    float t = 1;
    bool collided = false;
};

#endif // COLLISIONINFO

