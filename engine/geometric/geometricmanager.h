#ifndef GEOMETRICMANAGER_H
#define GEOMETRICMANAGER_H
#include <QList>
#include "engine/geometric/triangle.h"
#include "engine/geometric/ellipsoid.h"
#include "engine/core/entity.h"
#include "engine/geometric/collisioninfo.h"

class GeometricManager : public Manager
{
public:
    GeometricManager(Graphics *g, World *w);
    virtual ~GeometricManager();
    QList<Triangle*> triangles = QList<Triangle*>();

    Vector3 collide(Entity *e, Vector3 oldPosition, Vector3 potentialPosition);
    virtual void onTick(long seconds);
    virtual void onDraw(Graphics *g) = 0;
    virtual void loadTerrain();

protected:
    Vector3 currNormal;
    Vector3 entityPos;

};

#endif // GEOMETRICMANAGER_H
