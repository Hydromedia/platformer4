#ifndef NAVIGATIONTRIANGLE_H
#define NAVIGATIONTRIANGLE_H
#include "triangle.h"
#include "edge.h"
#include "engine/core/graphics.h"


class NavigationTriangle : public Triangle
{
public:
    NavigationTriangle(Vector3 v1, Vector3 v2, Vector3 v3, int first, int second, int third);
    ~NavigationTriangle();
    int a = 0;
    int b = 0;
    int c = 0;
    bool discovered = false;
    NavigationTriangle *prev = 0;
    Edge edgeToPrev = Edge(Vector3(), Vector3());

    Vector3 getLeftVertex(Edge e);
    Vector3 getRightVertex(Edge e);
};

#endif // NAVIGATIONTRIANGLE_H
