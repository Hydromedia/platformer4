#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "src/vector.h"

class Triangle
{
public:
    Triangle();
    // Vertices must be in CCW order
    Triangle(Vector3 v1, Vector3 v2, Vector3 v3);

    Vector3 centroid();
    Triangle scale(Vector3 basis);
    static bool isInTriangle(Triangle *tr, Vector3 P);

    Vector3 vertices[3];
    Vector3 normal;
};

#endif // TRIANGLE_H
