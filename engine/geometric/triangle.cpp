#include "triangle.h"
#include "stdio.h"

Triangle::Triangle(Vector3 v1, Vector3 v2, Vector3 v3){
    vertices[0] = v1;
    vertices[1] = v2;
    vertices[2] = v3;
    normal = (vertices[1] - vertices[0]).cross(vertices[2] - vertices[0]);
    normal.normalize();
}

Triangle Triangle::scale(Vector3 basis){
    return Triangle(vertices[0]*basis, vertices[1]*basis, vertices[2]*basis);
}

Vector3 Triangle::centroid(){
   return Vector3((vertices[0].x + vertices[1].x + vertices[2].x )/3,
                      (vertices[0].y + vertices[1].y + vertices[2].y )/3,
                      (vertices[0].z + vertices[1].z + vertices[2].z )/3);
}

bool Triangle::isInTriangle(Triangle *tr, Vector3 P)
{

    Vector3 A = tr->vertices[0];
    Vector3 B = tr->vertices[1];
    Vector3 C = tr->vertices[2];

    Triangle PAB = Triangle(P, A, B);
    Triangle PBC = Triangle(P, B, C);
    Triangle PCA = Triangle(P, C, A);

    //std::cout << "in" << std::endl;
    //std::cout << (P.x == P.x) << std::endl;
    if (P.x == P.x){
        //std::cout << "wtf" << std::endl;
        if (PAB.normal.dot(PBC.normal) > 0 && PBC.normal.dot(PCA.normal) > 0 &&
            PAB.normal.dot(PCA.normal) > 0){
            return true;
        }
    } else {
        return false;
    }
}
