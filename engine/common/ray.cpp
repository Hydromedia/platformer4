#include "ray.h"
#include <cmath>

Ray::Ray(Vector3 pos, Vector3 dir)
{
    position = pos;
    direction = dir;
}

Ray::~Ray()
{

}

CollisionInfo Ray::rayTriangle(Triangle *tr)
{
    Triangle tri = *tr;
    CollisionInfo info;
    info.collided = false;
    Vector3 N = tri.normal;
    Vector3 S = tri.vertices[0];
    float newT = (-1)*((N.dot(position-S))/(N.dot(direction)));
    //std::cout << "Norm: " << N << " A Point: " << S <<  " T2: " << newT << std::endl;
    Vector3 X = (position) + (direction)*newT;
    //std::cout << "P2: " << X << std::endl;
    if (tri.isInTriangle(&tri, X)){
        info.collided = true;
    }
    info.collisionPoint = X;
    info.normal = tri.normal;
    info.t = newT;
    //std::cout << "ended" << std::endl;
    return info;
}

CollisionInfo Ray::rayEllipsoidFromEllipsoidSpace(Ellipsoid *e)
{
    CollisionInfo info;

    Vector3 D = direction / e->radius;

    Vector3 A = e->start / e->radius;
    Vector3 B = A - D;

    Vector3 V = position / e->radius;

    float a = ((B-A)).lengthSquared();
    float b = (-2)*((B-A)).dot(V-A);
    float c = (V-A).lengthSquared() - 1;
    float newT1 = (-b + std::sqrt(b*b-4*a*c))/(2*a);
    float newT2 = (-b - std::sqrt(b*b-4*a*c))/(2*a);

    float det = (b*b-4*a*c);
    if(det >= 0) {


//        std::cout << "start: " << V << " direction: " << D << std::endl;
//        std::cout << "a: " << a << " b: " << b << std::endl;
//        std::cout << "c: " << c << " inside: " << (b*b-4*a*c) << " sqrt: " << std::sqrt(b*b-4*a*c) << std::endl;
//        std::cout << "t1: " << newT1 << std::endl;
//        std::cout << "t2: " << newT2 << std::endl;

        float newT = 1;
        if (newT1 > 0 && newT1 <= newT2) {
           //std::cout << "1" << std::endl;
            newT = newT1;
        } else if (newT2 > 0 && newT2 <= newT1) {
            newT = newT2;
            //std::cout << "2" << std::endl;
        } else {
            //std::cout << "Vert: They were both negative?" << std::endl;
        }

        Vector3 P = position;
        info.collisionPoint = (position) + (direction)*newT;
        info.t = newT;
    } else {
        //std::cout << "Determinant negative" << std::endl;
        info.t = -1;
    }


    return info;
}

CollisionInfo Ray::rayEllipsoid(Ellipsoid *e)
{
    CollisionInfo info;

    Vector3 A = e->start/e->radius;
    Vector3 B = e->end/e->radius;

    float a = ((B-A)).lengthSquared();
    float b = (-2)*((B-A)).dot(position-A);
    float c = (position-A).lengthSquared() - 1;
    float newT1 = (-b + std::sqrt(b*b-4*a*c))/(2*a);
    float newT2 = (-b - std::sqrt(b*b-4*a*c))/(2*a);

    float newT = 1;
    if (newT1 > 0 && newT1 <= newT2) {
        newT = newT1;
    } else if (newT2 > 0 && newT2 <= newT1) {
        newT = newT2;
    } else {
        //std::cout << "Vert: They were both negative?" << std::endl;
    }

    Vector3 P = position;
    info.collisionPoint = P;
    info.t = newT;
    return info;
}
