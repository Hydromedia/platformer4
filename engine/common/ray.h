#ifndef RAY_H
#define RAY_H
#include "src/vector.h"
#include "engine/geometric/collisioninfo.h"
#include "engine/geometric/triangle.h"
#include "engine/geometric/ellipsoid.h"

class Ellipsoid;
class Ray
{
public:
    Ray(Vector3 pos, Vector3 dir);
    ~Ray();
    Vector3 position;
    Vector3 direction;
    CollisionInfo rayTriangle(Triangle *tr);
    CollisionInfo rayEllipsoidFromEllipsoidSpace(Ellipsoid *e);
    CollisionInfo rayEllipsoid(Ellipsoid *e);
    CollisionInfo raySphere();

};

#endif // RAY_H
