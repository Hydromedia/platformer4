QT += core gui opengl

TARGET = warmup
TEMPLATE = app

# If you add your own folders, add them to INCLUDEPATH and DEPENDPATH, e.g.
 INCLUDEPATH += C:\Qt\5.4\mingw491_32\glew-1.12.0\include
 DEPENDPATH += C:\Qt\5.4\mingw491_32\glew-1.12.0\include

INCLUDEPATH += src
DEPENDPATH += src



#LIBS += -glut32 -glut -glew
LIBS += -lGLU

SOURCES += src/main.cpp \
    src/mainwindow.cpp \
    src/view.cpp \
    gameworld/gamescreen.cpp \
    gameworld/menuscreen.cpp \
    gameworld/gameapplication.cpp \
    engine/collision/shape.cpp \
    engine/collision/cylinder.cpp \
    gameworld/gameworld.cpp \
    gameworld/player.cpp \
    engine/voxel/voxelmanager.cpp \
    engine/voxel/chunk.cpp \
    engine/generation/perlinnoisegenerator3d.cpp \
    engine/core/application.cpp \
    engine/core/camera.cpp \
    engine/core/continuousphysicsmanager.cpp \
    engine/core/entity.cpp \
    engine/core/graphics.cpp \
    engine/core/manager.cpp \
    engine/core/screen.cpp \
    engine/core/world.cpp \
    engine/geometric/triangle.cpp \
    engine/geometric/ellipsoid.cpp \
    engine/common/ray.cpp \
    engine/geometric/geometricmanager.cpp \
    engine/core/resource.cpp \
    src/obj.cpp \
    gameworld/gamegeometricmanager.cpp \
    engine/geometric/trianglepather.cpp \
    gameworld/gametrianglepather.cpp \
    engine/geometric/navigationtriangle.cpp \
    engine/geometric/edge.cpp \
    gameworld/enemy.cpp

HEADERS += src/mainwindow.h \
    src/view.h \
    src/vector.h \
    gameworld/gamescreen.h \
    gameworld/menuscreen.h \
    gameworld/gameapplication.h \
    engine/collision/shape.h \
    engine/collision/cylinder.h \
    gameworld/gameworld.h \
    gameworld/player.h \
    engine/voxel/voxelmanager.h \
    engine/voxel/chunk.h \
    engine/voxel/block.h \
    gameworld/gamevoxelmanager.h \
    engine/generation/perlinnoisegenerator3d.h \
    engine/core/application.h \
    engine/core/camera.h \
    engine/core/continuousphysicsmanager.h \
    engine/core/entity.h \
    engine/core/graphics.h \
    engine/core/manager.h \
    engine/core/screen.h \
    engine/core/world.h \
    engine/geometric/triangle.h \
    engine/geometric/ellipsoid.h \
    engine/geometric/collisioninfo.h \
    engine/common/ray.h \
    engine/geometric/geometricmanager.h \
    engine/core/resource.h \
    src/obj.h \
    gameworld/gamegeometricmanager.h \
    engine/geometric/trianglepather.h \
    gameworld/gametrianglepather.h \
    engine/geometric/navigationtriangle.h \
    engine/geometric/edge.h \
    gameworld/enemy.h

FORMS += src/mainwindow.ui

RESOURCES += \
    texture1.qrc

OTHER_FILES += \
    res/level_island_navmesh_eroded.obj \
    res/level_island_navmesh.obj \
    res/level_island_channels.png \
    res/level_island.png \
    res/level_island.obj \
    res/level_hard_channels.png \
    res/level_hard.png \
    res/level_hard.obj \
    res/level_easy_navmesh.obj \
    res/level_easy_channels.png \
    res/level_easy.png \
    res/level_easy.obj
